var webpack = require('webpack');
var path = require('path');

var BUILD_DIR = path.resolve(__dirname, '../ViewTango_build/view/dist');
var APP_DIR = path.resolve(__dirname, 'src/app');

var PROD = JSON.parse(process.env.PROD_ENV || 0);

var config = {
    entry: APP_DIR + '/index.js',
    output: {
        path: BUILD_DIR,
        filename: 'bundle.js',
        publicPath: "/dist/"
    },
    module: {
        loaders: [
            {
                test: /\.jsx?/,
                include: APP_DIR,
                loader: 'babel-loader'
            },

            {
                test: /\.css$/,
                loader: 'style-loader!css-loader'
            }
        ],
    },
plugins: PROD ? [
    new webpack.DefinePlugin({
        'process.env': {
        'NODE_ENV': JSON.stringify('production')
        }
    }),
    new webpack.optimize.UglifyJsPlugin({
      compress: { warnings: false }
    })
]:[],

};

module.exports = config;
