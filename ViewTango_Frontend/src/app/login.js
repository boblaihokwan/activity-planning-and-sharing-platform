/*
*  CSCI3100 - Software engineering Project 2016-2017
*  Project Name: ViewTango 
*  Group: 8 (Diamond 8)
*
*  Filename: login.js
*  Description: To define a login view

*  All Rights Reserved.
*/

import React from 'react';
import ReactDOM from 'react-dom';
import { Button, ButtonToolbar, ControlLabel, FormControl, Col, Modal } from 'react-bootstrap'

export default class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <div>
                <Modal.Dialog show={this.state.showModal} onHide={this.close}>
                    <Col xs={8} md={8}>
                        <form>
                            <br></br>
                            <ControlLabel>Username:</ControlLabel>
                            <FormControl
                                type="text"
                                value={this.state.value}
                                placeholder=""
                                onChange={this.handleChange}
                            />
                            <FormControl.Feedback />
                            <ControlLabel>Password:</ControlLabel>
                            <FormControl
                                type="password"
                                value={this.state.value}
                                placeholder=""
                                onChange={this.handleChange}
                            />
                            <FormControl.Feedback />
                        </form>

                        <ButtonToolbar>
                            <Button bsStyle="primary" bsSize="large">Log in</Button>
                            <Button bsSize="large">Register</Button>
                        </ButtonToolbar>
                    </Col>
                    <Modal.Footer>
                    </Modal.Footer>
                </Modal.Dialog>
            </div>
        );
    }
}
