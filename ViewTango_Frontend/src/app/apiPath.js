/*
*  CSCI3100 - Software engineering Project 2016-2017
*  Project Name: ViewTango 
*  Group: 8 (Diamond 8)
*
*  Filename: apiPath.js
*  Description: To define the API url path.

*  All Rights Reserved.
*/

export const apiPath = {
    auth: `http://viewtango-testdatabase.rhcloud.com/api/auth`,
    user: `http://viewtango-testdatabase.rhcloud.com/api/user`,
    activity: `http://viewtango-testdatabase.rhcloud.com/api/activity`,
    propic: `http://viewtango-testdatabase.rhcloud.com/public/propic/`,
    category: `http://viewtango-testdatabase.rhcloud.com/api/category`,
    activityPic: `http://viewtango-testdatabase.rhcloud.com/public/activity/`,
    staticGMap:`https://maps.googleapis.com/maps/api/staticmap?maptype=roadmap&key=AIzaSyC9wy-2QavFd4U2xrn9nOpH59wyDyWJ6Ck`,
    iconPath: `http://viewtango-testdatabase.rhcloud.com/src/assets/`
}
