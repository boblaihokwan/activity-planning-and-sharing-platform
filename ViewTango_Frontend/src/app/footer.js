/*
*  CSCI3100 - Software engineering Project 2016-2017
*  Project Name: ViewTango 
*  Group: 8 (Diamond 8)
*
*  Filename: footer.js
*  Description: To define a the footer view.

*  All Rights Reserved.
*/

import React from 'react';
import { Modal, Button, Panel, ButtonToolbar } from 'react-bootstrap';
import { ShareButtons, ShareCounts, generateShareIcon } from 'react-share';

const FacebookIcon = generateShareIcon('facebook');
const TwitterIcon = generateShareIcon('twitter');
const GooglePlusIcon = generateShareIcon('google');
const shareUrl = 'http://viewtango-testdatabase.rhcloud.com/';
const title = 'Join ViewTango Today!'

const {
  FacebookShareButton,
    GooglePlusShareButton,
    TwitterShareButton,
    PinterestShareButton,
} = ShareButtons;

const footerInstance = (
    <div>
        <br/>
        <p className="pull-right">Copyright © 2017 ViewTango. All rights reserved.</p>
        <table>
            <tr>
                <td>
                    <FacebookShareButton size={32} url={shareUrl} title={title}>
                        <FacebookIcon size={32} />
                    </FacebookShareButton>
                </td>
                <td>
                    <GooglePlusShareButton size={32} url={shareUrl} title={title}>
                        <GooglePlusIcon size={32} />
                    </GooglePlusShareButton>
                </td>
                <td>
                    <TwitterShareButton size={32} url={shareUrl} title={title}>
                        <TwitterIcon size={32} />
                    </TwitterShareButton>
                </td>
            </tr>
        </table>
        <br/>
    </div>
);

export default class Footer extends React.Component {
    render() {
        return (footerInstance)
    }
}