/*
*  CSCI3100 - Software engineering Project 2016-2017
*  Project Name: ViewTango 
*  Group: 8 (Diamond 8)
*
*  Filename: main.js
*  Description: To define application main view (e.g. path routing)

*  All Rights Reserved.
*/

import React from 'react';
import { render } from 'react-dom'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as action from './action/action';
import Homebar from './homebar'
import Homecontain from './homecontain'
import Footer from './footer'
import Login from './login'
import Register from './register'
import Profile from './profile'
require("../css/custom.css");

class Main extends React.Component {

    constructor() {
        super();
    }

    render() {
        console.log(this.props.store);
        return (
            <div>
                <Homebar store={this.props.store} actions={this.props.actions} />
                {this.props.children}
                <Footer />
            </div>
        )
    }
}

function mapStateToProps(state, ownProps) {
    return {
        store: state.combined.auth,
        view: state.combined.view
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(action, dispatch),
        viewActions: bindActionCreators(action, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Main);
