/*
*  CSCI3100 - Software engineering Project 2016-2017
*  Project Name: ViewTango 
*  Group: 8 (Diamond 8)
*
*  Filename: time.js
*  Description: To define a activity information view with timeline.

*  All Rights Reserved.
*/

  // import library
import React from 'react';
import ReactDOM from 'react-dom';
import { ul, li, a, i, ProgressBar, circle, Overlay, OverlayTrigger, strong, Label, Tooltip, MenuItem, InputGroup, ButtonGroup, DropdownButton, Image, img, Glyphicon, Table, thead, title, span, Button, ButtonToolbar, Form, FormGroup, FormControl, Modal, ControlLabel, Grid, Row, Col, head, ListGroupItem, ListGroup, Panel } from 'react-bootstrap';
import '../css/timeline.css';
import * as action from './action/action';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import FaCircleo from 'react-icons/lib/fa/circle';
import ActivityModel from './activityModal';
import ProfilePicModal from './ProfilePicModal'
import TimeAgo from 'react-timeago'
import { Link, browserHistory, history, createHistory } from 'react-router'
import Gmap from './map'
import { apiPath } from './apiPath'
import Moment from 'react-moment'
import { ShareButtons, ShareCounts, generateShareIcon } from 'react-share';

//Define the path of API
var propicapipath = apiPath.propic
var activitypath = apiPath.activityPic

const FacebookIcon = generateShareIcon('facebook');
const TwitterIcon = generateShareIcon('twitter');
const GooglePlusIcon = generateShareIcon('google');

const {
  FacebookShareButton,
    GooglePlusShareButton,
    TwitterShareButton,
    PinterestShareButton,
} = ShareButtons;

var like = 0;
var comment = null

var NewComponent = React.createClass({
    render: function () {
        return (
            <div>
                <ul className="timeline">
                    <li className="timeline-inverted">
                        <div className="timeline-badge">
                            <Label><FaCircleo />{" "}</Label><i className="react-icons/lib/fa/circle-o" id />
                        </div>
                        <div className="timeline-panel">
                            <div className="timeline-heading">
                                <h4>Timeline Event</h4>
                            </div>
                            <div className="timeline-body">
                                <p>Pizza is handsome</p>
                            </div>
                            <div className="timeline-footer">
                                <p className="text-right">Feb-21-2014</p>
                            </div>
                        </div>
                    </li>
                    <li className="timeline-inverted">
                        <div className="timeline-badge">
                            <a><i className="fa fa-circle invert" id /></a>
                        </div>
                        <div className="timeline-panel">
                            <div className="timeline-heading">
                                <h4>Timeline Event</h4>
                            </div>
                            <div className="timeline-body">
                                <p>Picture 2 </p>
                            </div>
                            <div className="timeline-footer">
                                <p className="text-right">Feb-23-2014</p>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div className="timeline-badge">
                            <a><i className="fa fa-circle" id /></a>
                        </div>
                        <div className="timeline-panel">
                            <div className="timeline-heading">
                                <h4>Timeline Event</h4>
                            </div>
                            <div className="timeline-body">
                                <p>picture pls</p>
                            </div>
                            <div className="timeline-footer">
                                <p className="text-right">Feb-23-2014</p>
                            </div>
                        </div>
                    </li>
                    <li className="timeline-inverted">
                        <div className="timeline-badge">
                            <a><i className="fa fa-circle invert" id /></a>
                        </div>
                        <div className="timeline-panel">
                            <div className="timeline-heading">
                                <h4>Timeline Event</h4>
                            </div>
                            <div className="timeline-body">
                                <p>picture 2</p>
                            </div>
                            <div className="timeline-footer">
                                <p className="text-right">Feb-27-2014</p>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div className="timeline-badge">
                            <a><i className="fa fa-circle" id /></a>
                        </div>
                        <div className="timeline-panel">
                            <div className="timeline-heading">
                                <h4>Timeline Event</h4>
                            </div>
                            <div className="timeline-body">
                                <p>picture4</p>
                            </div>
                            <div className="timeline-footer">
                                <p className="text-right">Mar-01-2014</p>
                            </div>
                        </div>
                    </li>
                    <li className="timeline-inverted">
                        <div className="timeline-badge">
                            <a><i className="fa fa-circle invert" id /></a>
                        </div>
                        <div className="timeline-panel">
                            <div className="timeline-heading">
                                <h4>Timeline Event</h4>
                            </div>
                            <div className="timeline-body">
                                <p>picture5</p>
                            </div>
                            <div className="timeline-footer primary">
                                <p className="text-right">Mar-02-2014</p>
                            </div>
                        </div>
                    </li>
                    <li className="clearfix no-float" />
                </ul>
            </div>
        );
    }
});

var UploadModal = React.createClass({

    getInitialState: function () {
        return {
            editingId: "0"
        }
    },

    onEditItem: function (event) {
        // I want to pass the ID to modal and open it
        this.setState({ editingId: event.target.id });
        this.props.actions.openProfilePicModal()
    },

    render: function () {
        return (
            <div>
                <Button id="2" bsStyle="link" className="pull-left" bsSize="xsmall" onClick={this.onEditItem} active>
                    <Glyphicon glyph="picture" />
                </Button>
                <ProfilePicModal show={this.props.store.openProfilePicModal} onHide={this.props.actions.closeProfilePicModal} actions={this.props.actions} store={this.props.store} act="ACTIVITY" />
            </div>
        )
    }
});

const timeline = React.createClass({

    getInitialState: function () {
        var i = 0;
        this.props.actions.indactivity({
            type: 'ACTIVITY',
            value: '/' + this.props.params.id
        });
        return {
            loadingActivity: true,
            activity: null,
            likeed: false,
            hated: false,
            comment: "",
            user: this.props.store.user,
            editingId: 0
        }
    },

    handleActivity: function (id) {
        // dispatch action
        this.props.actions.activity({
            type: 'ACTIVITY',
            value: '/' + id
        });
    },

    handleChange: function (event) {
        if (event.target.id === "comment") {
            this.setState({
                comment: event.target.value
            })
        }
        comment = event.target.value;
    },

    handleComment: function () {
        if (comment === "") {
            this.setState({
                missingMsg: "Please leave your comment"
            })
        } else {
            this.props.actions.addactivityCon({
                type: 'ACTIVITY',
                value: {
                    content: {
                        content: comment,
                        author: this.state.user
                    },
                    path: '/' + this.state.activity._id + '/comment',
                    id: this.state.activity._id,
                    activity: this.state.activity
                }
            });
            this.setState({
                missingMsg: null,
                comment: ""
            })
        }
    },

    handleRate: function (event) {
        // dispatch action
        if (event.target.id === "like") {
            this.setState({
                liked: true
            })
        } else {
            this.setState({
                hated: true
            })
        }

        this.props.actions.addactivityRate({
            type: 'ACTIVITY',
            value: {
                path: '/' + this.state.activity._id + '/' + event.target.id,
                id: this.state.activity._id,
                activity: this.state.activity
            }
        });
    },

    onEditItem: function (event) {
        // I want to pass the ID to modal and open it
        this.setState({
            editingId: event.target.id
        });
        this.props.actions.openProfilePicModal()
    },

    getActivity: function () {

        this.state = {
            activity: this.props.store.activity,
            editingId: this.state.editingId
        }

        //Handle Rating
        var total, likeAvg = 0;
        var starIn = [];
        total = this.state.activity.likes + this.state.activity.dislikes;
        likeAvg = (this.state.activity.likes / total) * 5;
        for (var j = 0; j < 5; j++) {
            if (j <= likeAvg) {
                starIn.push(<Glyphicon glyph="star" />);
            } else {
                starIn.push(<Glyphicon glyph="star-empty" />);
            }
        }

        //Handle Owner button
        var ownerstates, editactivity = "";
        var login, rated, owner = false;
        if (this.props.store.status === 'logged in') {
            login = true
            if (this.props.store.user === this.state.activity.author.username) {
                rated = true;
                owner = true;
                ownerstates = (
                    <ButtonGroup vertical block>
                        <Button id="POST" bsStyle="primary" bsSize="Clear" onClick={() => this.props.actions.openActivityModal()} active>Edit Activity</Button>
                    </ButtonGroup>
                )
                editactivity = (
                    <div>
                        <ActivityModel show={this.props.store.openActivityModal} onHide={this.props.actions.closeActivityModal} actions={this.props.actions} store={this.props.store} id="PUT" />
                    </div>
                )
            } else if (this.props.store.isLikedOrDisliked) {
                rated = true;
            }
        } else if (this.props.store.user === 'Guest') {
            login = false;
            rated = true;
        }

        //Handle Timeline
        var TimelineObject = [];
        for (var i = 0; i < this.state.activity.contents.length; i++) {
            var timeline = "";

            if (this.state.activity.contents[i].imageUrl !== "") {
                var actpic = (
                    <p>
                        <Image src={activitypath + this.state.activity.contents[i].imageUrl} alt="My Picture" width="100%" height="200" rounded />
                    </p>
                )
            } else {
                actpic = ""
            }
            if (i % 2 == 0) {
                TimelineObject.push(
                    <li className="timeline-inverted">
                        <div className="timeline-badge">
                            <div className="icon">
                                <FaCircleo />
                            </div>
                        </div>
                        <div className="timeline-panel">
                            <div className="timeline-heading">
                                <h4>{this.state.activity.contents[i].title}</h4>
                            </div>
                            <div className="timeline-body">
                                <p>{this.state.activity.contents[i].description}</p>
                                {actpic}
                            </div>
                            <div className="timeline-footer">
                                <p className="text-right">
                                    {this.state.activity.contents[i].time}{" "}
                                    {owner ?
                                        <Button id={i} bsStyle="link" className="pull-left" bsSize="xsmall" onClick={this.onEditItem} active>
                                            <Glyphicon id={i} glyph="picture" />
                                        </Button>
                                        : null}
                                    <div className="clearfix"></div>
                                </p>
                            </div>
                        </div>
                    </li>
                )
            } else {
                TimelineObject.push(
                    <li>
                        <div className="timeline-badge icon">
                            <div className="icon">
                                <FaCircleo />
                            </div>
                        </div>
                        <div className="timeline-panel">
                            <div className="timeline-heading">
                                <h4>{this.state.activity.contents[i].title}</h4>
                            </div>
                            <div className="timeline-body">
                                <p>{this.state.activity.contents[i].description}</p>
                                {actpic}
                            </div>
                            <div className="timeline-footer">
                                <p className="text-right">
                                    {this.state.activity.contents[i].time}{" "}
                                    {owner ?
                                        <Button id={i} bsStyle="link" className="pull-left" bsSize="xsmall" disabled={!owner} onClick={this.onEditItem} active>
                                            <Glyphicon id={i} glyph="picture" />
                                        </Button>
                                        : null}
                                    <div className="clearfix"></div>
                                </p>
                            </div>
                        </div>
                    </li>
                )
            }
        }

        //Handel Comment
        var commentIn = [];
        var propic;

        for (var j = 0; j < this.state.activity.comments.length; j++) {
            //If find propic then use; other set default
            if (this.state.activity.comments[j].author.Profile.propic !== "") {
                propic = propicapipath + this.state.activity.comments[j].author.Profile.propic
            } else {
                propic = "/src/assets/profile-icon.jpg"
            }
            commentIn.push(
                <tr>
                    <td className="col-md-1">
                        <Image width="40" height="40" src={propic} circle />
                    </td>
                    <td clasNames="col-md-8">
                        <p><b>{this.state.activity.comments[j].author.username}</b></p>
                        {this.state.activity.comments[j].content}
                    </td>
                    <td className="col-md-3"><small><Glyphicon glyph="time" />{" "}<TimeAgo date={this.state.activity.comments[j].createdAt} /></small></td>
                </tr >
            )
        }

        this.state = {
            starIn: starIn,
            activity: this.state.activity,
            categories: this.state.activity.categories,
            ownerstates: ownerstates,
            TimelineObject: TimelineObject,
            liked: rated,
            hated: rated,
            login: login,
            user: this.props.store.user,
            comment: comment,
            editactivity: editactivity,
            commentIn: commentIn,
            editingId: this.state.editingId
        }
    },

    render() {
        if (this.props.store.isLoadingIndActivity) {
            var loadingmodal = (
                <Modal.Dialog >
                    <Modal.Body><h4>It just loading...</h4>
                        <ProgressBar active now={100} />
                    </Modal.Body>
                </Modal.Dialog >
            );
        }

        if (this.props.store.activity && this.props.store.activity._id) {
            this.getActivity();
            const shareUrl = 'http://viewtango-testdatabase.rhcloud.com/activity/' + this.state.activity._id;
            const titleevent = 'Join ViewTango Event - ' + this.state.activity.title;
            var actView = (
                <div>
                    <ProfilePicModal show={this.props.store.openProfilePicModal} onHide={this.props.actions.closeProfilePicModal} actions={this.props.actions} store={this.props.store} act="ACTIVITY" id={this.state.editingId} />
                    {this.state.editactivity}
                    <Panel>
                        <Col xs={8}>
                            <Panel>
                                <ListGroup fill>
                                    <ListGroupItem>
                                        <h3><b>{this.state.activity.title}</b></h3>
                                        <h3><small>{" Create by "}{this.state.activity.author.username}</small></h3>
                                        <Table>
                                            <tbody>
                                                <tr>
                                                    <td className="col-md-2"><h7><b>Date</b></h7></td>
                                                    <td className="col-md-4"><Moment format="YYYY/MM/DD">{this.state.activity.date}</Moment></td>
                                                </tr>
                                                <tr>
                                                    <td className="col-md-2"><h7><b>Place</b></h7></td>
                                                    <td className="col-md-4">{this.state.activity.location}</td>
                                                    <td className="col-md-2"><h7><b>People Need</b></h7></td>
                                                    <td className="col-md-4">{this.state.activity.peopleneed}</td>
                                                </tr>
                                                <tr>
                                                    <td><h7><b>Rating</b></h7></td>
                                                    <td>{this.state.starIn}</td>
                                                    <td><h7><b>Catagory</b></h7></td>
                                                    <td>{this.state.activity.categories}</td>
                                                </tr>
                                            </tbody>
                                        </Table>
                                        <Table>
                                            <tbody>
                                                <tr>
                                                    <td colSpan="2" className="col-md-2"><h7><b>Description</b></h7></td>
                                                    <td className="col-md-10">{this.state.activity.description}</td>
                                                </tr>
                                            </tbody>
                                        </Table>
                                        <p>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <FacebookShareButton size={32} url={shareUrl} title={titleevent}>
                                                            <FacebookIcon size={32} />
                                                        </FacebookShareButton>
                                                    </td>
                                                    <td>
                                                        <GooglePlusShareButton size={32} url={shareUrl} title={titleevent}>
                                                            <GooglePlusIcon size={32} />
                                                        </GooglePlusShareButton>
                                                    </td>
                                                    <td>
                                                        <TwitterShareButton size={32} url={shareUrl} title={titleevent}>
                                                            <TwitterIcon size={32} />
                                                        </TwitterShareButton>
                                                    </td>
                                                </tr>
                                            </table>
                                            <ButtonToolbar>
                                                <Button id="dislike" onClick={this.handleRate} className="pull-right" bsStyle="warning" bsSize="small" disabled={this.state.hated}><Glyphicon id="dislike" glyph="thumbs-down" />{" "}{this.state.activity.dislikes} DisLike</Button>
                                                <Button id="like" onClick={this.handleRate} className="pull-right" bsStyle="danger" bsSize="small" disabled={this.state.liked}><Glyphicon id="like" glyph="thumbs-up" />{" "}{this.state.activity.likes} Like</Button>
                                            </ButtonToolbar>
                                        </p>
                                        <center>
                                            {this.state.ownerstates}
                                        </center>
                                    </ListGroupItem>
                                </ListGroup>
                            </Panel>
                            <ul className="timeline">
                                {this.state.TimelineObject}
                                <li className="clearfix no-float" />
                            </ul>
                            <Panel>
                                <Table style={{ overflow: 'auto' }} striped>
                                    <tbody>
                                        <thead>
                                            <tr>
                                                <th>Comment</th>
                                            </tr>
                                        </thead>
                                        {this.state.commentIn}
                                    </tbody>
                                </Table>
                                <div>
                                    <FormGroup>
                                        <InputGroup>
                                            <FormControl id="comment" type="text" disabled={!this.state.login} value={this.state.comment} onChange={this.handleChange} />
                                            <InputGroup.Button>
                                                <Button disabled={!this.state.login} onClick={this.handleComment}>Send</Button>
                                            </InputGroup.Button>
                                        </InputGroup>
                                    </FormGroup>
                                    <Label bsStyle="default"><small>Send as {this.state.user}</small></Label>
                                </div>
                            </Panel>
                        </Col>
                        <Col xs={4}>
                            <Col xs={12} md={12}>
                                <Gmap actions={this.props.actions} store={this.props.store} width={300} height={650} location={this.state.location} auto={false} />
                            </Col>
                            <Col xs={12} md={12}>
                                <Panel>
                                </Panel>
                            </Col>
                        </Col>
                    </Panel>
                </div >
            )
        }
        return (
            <div>
                {loadingmodal}
                {actView}
            </div>
        );
    }
})

timeline.contextTypes = {
    store: React.PropTypes.object.isRequired
};

function mapStateToProps(state, ownProps) {
    return {
        store: state.combined.auth,
        view: state.combined.view
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(action, dispatch),
        viewActions: bindActionCreators(action, dispatch)
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(timeline);
