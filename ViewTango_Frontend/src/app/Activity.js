/*
*  CSCI3100 - Software engineering Project 2016-2017
*  Project Name: ViewTango 
*  Group: 8 (Diamond 8)
*
*  Filename: Activity.js
*  Description: To list the activity.

*  All Rights Reserved.
*/

import React from 'react';
import ReactDOM from 'react-dom';
import { Tabs, Tab, Panel, Modal, Button, ProgressBar, ButtonToolbar, Glyphicon, Row, Col, Grid, FormGroup, InputGroup, FormControl, Form } from 'react-bootstrap'
import * as action from './action/action';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Items from './activities/activityItem';
import ActivityModel from './activityModal'
import Timeline from './time'

var searchClick = false;
var keyword;

const AddActivityButton = React.createClass({
    render() {
        return (
            <Button className="pull-right" bsStyle="danger" onClick={() => this.props.actions.openActivityModal()}><Glyphicon glyph="flag" /> Add Activity </Button>
        );
    }
});

const SearchActivityButton = React.createClass({

    getInitialState: function () {
        return {
            keyword: null
        }
    },

    handleSrcActivity: function () {
        // dispatch action
        this.props.actions.activity({
            type: 'ACTIVITY',
            value: "/search?keyword=" + this.state.keyword
        });
        searchClick = true;
    },

    handleChange: function (event) {
        if (event.target.id === 'keyword') {
            this.setState({
                keyword: event.target.value
            })
        }
    },

    render() {
        return (
            <Form className="pull-right" inline>
                <FormGroup>
                    <InputGroup>
                        <FormControl id="keyword" type="text" onChange={this.handleChange} />
                        <InputGroup.Button>
                            <Button onClick={this.handleSrcActivity}>
                                <Glyphicon glyph="search" />
                            </Button>
                        </InputGroup.Button>
                    </InputGroup>
                </FormGroup>
            </Form>
        );
    }
});

const Activity = React.createClass({

    handleActivity: function (eventKey, event) {
        // dispatch action
        this.state = {
            type: eventKey,
            searchClick: false
        }
        this.props.actions.activity({
            type: 'ACTIVITY',
            value: eventKey
        });
        event.target.activeKey = eventKey;
    },

    handleBackActivity: function () {
        // dispatch action
        this.state = {
            searchClick: false
        }
        this.props.actions.activity({
            type: 'ACTIVITY',
            value: "/?sort=latest"
        });
    },


    handleSrcActivity: function () {
        // dispatch action
        this.props.actions.activity({
            type: 'ACTIVITY',
            value: "/search?keyword=" + this.state.keyword
        });
        this.setState({
            searchClick: true,
            keyword: ""
        })
        //console.log(this.props.state)
    },

    handleChange: function (event) {
        if (event.target.id === 'keyword') {
            this.setState({
                keyword: event.target.value,
            })
            keyword = this.state.keyword
        }
    },

    getInitialState: function () {
        var i = 0;
        var loadingmodal = (
            <Modal.Dialog >
                <Modal.Body><h4>It just loading.</h4></Modal.Body>
            </Modal.Dialog >
        );

        this.props.actions.activity({
            type: 'ACTIVITY',
            value: "/?sort=latest"
        });

        if (!this.props.store.activity) {
            return {
                loadingActivity: loadingmodal,
                loginstates: '',
                searchClick: false
            }
        } else {
            return {
                loadingActivity: '',
                searchClick: false
            }
        }
    },

    render() {

        var loginstates;


        if (this.props.store.status === 'logged in') {
            loginstates = (
                <AddActivityButton actions={this.props.actions} store={this.props.store} />
            )
        }
        var noResult = null;

        if (!this.props.store.activity || this.props.store.activity.length === 0 || this.props.store.activity === undefined) {
            noResult = (
                <Panel><h4>No Matching Result</h4></Panel>
            )
        }

        if (this.props.store.activity) {
            var actView = (
                <div>
                    <p>
                        <ButtonToolbar>
                            {loginstates}
                            <Form className="pull-right" inline>
                                <FormGroup>
                                    <InputGroup>
                                        <FormControl id="keyword" value={this.state.keyword} type="text" onChange={this.handleChange} />
                                        <InputGroup.Button>
                                            <Button onClick={this.handleSrcActivity}>
                                                <Glyphicon glyph="search" />
                                            </Button>
                                        </InputGroup.Button>
                                    </InputGroup>
                                </FormGroup>
                            </Form>
                        </ButtonToolbar>
                    </p>
                    {
                        !this.state.searchClick ?
                            <Tabs animation={false} defaultActiveKey="/?sort=latest" id="noanim-tab-example" bsStyle="tabs" style={{ backgroundColor: "#FFFFFF" }} onSelect={this.handleActivity}>
                                <Tab eventKey="/?sort=latest" title="Latest" >
                                    <Items actions={this.props.actions} store={this.props.store} />
                                </Tab>
                                <Tab eventKey="/?sort=mostViewed" title="Most Viewed" >
                                    <Items actions={this.props.actions} store={this.props.store} />
                                </Tab>
                            </Tabs> :
                            <div>
                                <h3>Search Result</h3>
                                {noResult}
                                <Items actions={this.props.actions} store={this.props.store} />
                                <Button onClick={this.handleBackActivity}>Back</Button>
                            </div>
                    }
                    <ActivityModel id="POST" show={this.props.store.openActivityModal} onHide={this.props.actions.closeActivityModal} actions={this.props.actions} store={this.props.store} />
                </div>
            )
        }

        if (this.props.store.isLoadingActivity) {
            var loadingmodal = (
                <Modal.Dialog >
                    <Modal.Body><h4>It's just loading...</h4>
                        <ProgressBar active now={100} />
                    </Modal.Body>
                </Modal.Dialog >
            );
        }
        return (
            <div>
                {loadingmodal}
                {actView}
            </div>
        )

    }
})

Activity.contextTypes = {
    store: React.PropTypes.object.isRequired
};

function mapStateToProps(state, ownProps) {
    return {
        store: state.combined.auth,
        view: state.combined.view
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(action, dispatch),
        viewActions: bindActionCreators(action, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Activity);
