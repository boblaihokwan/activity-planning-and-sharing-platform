/*
*  CSCI3100 - Software engineering Project 2016-2017
*  Project Name: ViewTango 
*  Group: 8 (Diamond 8)
*
*  Filename: map.js
*  Description: To define a map view.

*  All Rights Reserved.
*/

import React from 'react';
import ReactDOM from 'react-dom';
import { ButtonToolbar, Button, FormGroup, Form, FormControl, InputGroup } from 'react-bootstrap';
import Autocomplete from 'react-google-autocomplete'

var map, mapOptions;
var geocoder = new google.maps.Geocoder();

var ButtonFunction = React.createClass({

  show: function () {
    geocoder.geocode({ 'address': this.props.myDataProp }, function (results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        map.setZoom(20);
        map.setCenter({ lat: results[0].geometry.location.lat(), lng: results[0].geometry.location.lng() });
      }
      else {
        alert("Please enter more clear place name!")
      }

    });
  },
  render() {
    return (
      <div>
        <FormGroup>
          <InputGroup>
            <FormControl type="text" placeholder="Enter a place!" value={this.props.myDataProp} onChange={this.props.updateStateProp} />
          </InputGroup>
        </FormGroup>
        <ButtonToolbar>
          <Button bsStyle="primary" bsSize="small" onClick={this.show} >Submit</Button>
        </ButtonToolbar>
      </div>
    );
  }
}
);



mapOptions = {
  center: { lat: -34.397, lng: 150.644 },
  zoom: 5,
  scaleControl: true,
  mapTypeControl: true,
  mapTypeId: google.maps.MapTypeId.ROADMAP,
  mapTypeControlOptions: {
    style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
    position: google.maps.ControlPosition.TOP_CENTER,
    mapTypeIds: [
      google.maps.MapTypeId.ROADMAP,
      google.maps.MapTypeId.TERRAIN,
      google.maps.MapTypeId.HYBRID
    ]
  },

  streetViewControl: true,
  streetViewControlOptions: {
    position: google.maps.ControlPosition.RIGHT_CENTER
  },


};

export default class Gmap extends React.Component {
  componentDidMount() {
    map = new google.maps.Map(document.getElementById('map'), mapOptions);
  }
  constructor(props) {
    super(props);

    this.state = {
      address: this.props.location
    }

    this.updateState = this.updateState.bind(this);
  };

  show() {
    geocoder.geocode({ 'address': this.props.store.activity.location }, function (results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        map.setZoom(17);
        map.setCenter({ lat: results[0].geometry.location.lat(), lng: results[0].geometry.location.lng() });
      }
    });
  }

  updateState() {
    this.setState({ address: 'Sai Kung' });
  }
  render() {
    this.show()
    var width = this.props.width;
    var height = this.props.height;
    console.log(this.props.location)
    return (
      <div>
        <div style={{ width: width, height: height }} id="map"></div>
      </div>
    );
  }
}
