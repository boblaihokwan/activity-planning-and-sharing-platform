/*
*  CSCI3100 - Software engineering Project 2016-2017
*  Project Name: ViewTango 
*  Group: 8 (Diamond 8)
*
*  Filename: homecontain.js
*  Description: To define a homepage view.

*  All Rights Reserved.
*/

import React from 'react';
import { Carousel, Col, Grid, Row, Panel, Modal, ProgressBar, Glyphicon } from 'react-bootstrap';
import style from '../css/custom.css';
import * as action from './action/action';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { apiPath } from './apiPath'

//Define the path of API
var activitypath = apiPath.activityPic
var staticgmap = apiPath.staticGMap

var place = ['Tsim Sha Tsui', 'Sha Tin', 'Tai Po', 'Sai Kung'];

var carousels = [];
for (var i = 0; i < 4; i++) {
    carousels.push(
        <Carousel.Item>
            <div className={style.slind}>
                <img src="src/assets/test2.jpg" style={{
                    width: '100%', height: 'auto'
                    , margin: 'auto', position: 'relative', top: '0', bottom: '0'
                }} />
            </div>
            <Carousel.Caption>
                <h3>{place[i]}</h3>
                <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
            </Carousel.Caption>
        </Carousel.Item>
    );
}

var cards = [];
for (var i = 0; i < 4; i++) {
    cards.push(
        <Col sm={6} md={3}>
            <div className={style.card}>
                <img src="src/assets/test2.jpg" alt="Avatar" style={{ width: '100%' }} />
                <div className={style.container}>
                    <h4><b>{place[i]}</b></h4>
                    <p>{place[i]}</p>
                </div>
            </div>
        </Col>
    );
}

const Homecontain = React.createClass({
    getInitialState: function () {
        this.props.actions.activity({
            type: 'ACTIVITY',
            value: "/?sort=latest"
        });
        return {

        }
    },
    render() {
        if (this.props.store.isLoadingActivity === true) {
            var loadingmodal = (
                <Modal.Dialog >
                    <Modal.Body><h4>It's just loading...</h4>
                        <ProgressBar active now={100} />
                    </Modal.Body>
                </Modal.Dialog >
            );
        } else {
            loadingmodal = null;
        }

        if (this.props.store.isLoadingActivity === false && this.props.store.activity.length > 0) {
            var carousels = [];
            var count;
            if (this.props.store.activity.length <= 5) {
                count = this.props.store.activity.length;
            } else {
                count = 5;
            }
            for (var i = 0; i < count; i++) {
                var photopath = null;
                for (var j = 0; j < this.props.store.activity[i].contents.length; j++) {
                    if (this.props.store.activity[i].contents[j].imageUrl !== "") {
                        photopath = activitypath + this.props.store.activity[i].contents[j].imageUrl
                        break;
                    }
                }
                if (photopath === null) {
                    photopath = staticgmap + "&zoom=16&size=640x400&center=" + this.props.store.activity[i].location
                }
                carousels.push(
                    <Carousel.Item>
                        <div className={style.slind}>
                            <img className="img-responsive center-block" src={photopath} style={{
                                width: 'auto', height: '500'
                                , margin: '0 auto', position: 'relative', top: '0', bottom: '0'
                            }} />
                        </div>
                        <Carousel.Caption>
                            <h3>{this.props.store.activity[i].title}</h3>
                            <p>{this.props.store.activity[i].description}</p>
                        </Carousel.Caption>
                    </Carousel.Item>
                );
            }
        }
        return (
            <div>
                {loadingmodal}
                <div>
                    <Panel width='50%'>
                        <Carousel>
                            {carousels}
                        </Carousel>
                    </Panel>
                    <p></p>
                    <Panel width='50%' bsStyle="success">
                        <center>
                            <h3>Create Your Activity Today!</h3>
                        </center>
                        <Col xs={6} md={4}>
                        <center>
                            <h1><Glyphicon glyph="user" style={{ color: "#337ab7" }} /></h1>
                            <h3>Meet New Friend</h3>
                            <h5>Find new friends based on common interests, event and location.</h5>
                        </center>
                        </Col>
                        <Col xs={6} md={4}>
                        <center>
                            <h1><Glyphicon glyph="heart" style={{ color: "#337ab7" }} /></h1>
                            <h3>Develop Your Interests</h3>
                            <h5>Find your interest in camera. Impove your skill by appreciating people work.</h5>
                        </center>
                        </Col>
                        <Col xsHidden md={4}>
                        <center>
                            <h1><Glyphicon glyph="camera" style={{ color: "#337ab7" }} /></h1>
                            <h3>Take Your Camera Now</h3>
                            <h5>Join us now and create activity to open your amazing joerney!</h5>
                        </center>
                        </Col>
                    </Panel>
                </div>
            </div>
        )
    }
})

Homecontain.contextTypes = {
    store: React.PropTypes.object.isRequired
};

function mapStateToProps(state, ownProps) {
    return {
        store: state.combined.auth,
        view: state.combined.view
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(action, dispatch),
        viewActions: bindActionCreators(action, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Homecontain);
