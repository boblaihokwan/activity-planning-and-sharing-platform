/*
*  CSCI3100 - Software engineering Project 2016-2017
*  Project Name: ViewTango 
*  Group: 8 (Diamond 8)
*
*  Filename: auth.js
*  Description: To create a reducers on basic function.

*  All Rights Reserved.
*/

import { actionType } from '../action/actionType'
import { Link, browserHistory, history, createHistory } from 'react-router'

var success = true;

export default function auth(state = {
    status: 'logged out',
    user: 'Guest'
}, action) {

    switch (action.type) {
        case actionType.REQUEST_PROFILE:
            return Object.assign({}, state, {
                user: action.user,
                success: action.success,
                profile: action.profile,
            })
        case actionType.REQUEST_LOGIN:
            return Object.assign({}, state, {
                status: 'logging in',
                user: action.user,
                success: action.success,
            })
        case actionType.REQUEST_LOGOUT:
            return Object.assign({}, state, {
                status: 'logging out',
                user: action.user,
                success: action.success,
            })
        case actionType.REQUEST_EDITPROFILE:
            return Object.assign({}, state, {
                status: 'editting profile',
                user: action.user,
                profile: action.profile,
                success: action.success,
            })
        case actionType.REQUEST_EDITPROFILEPIC:
            return Object.assign({}, state, {
                status: 'editting profile Pic',
                user: action.user,
                profile: action.profile,
                success: action.success,
            })
        case actionType.REQUEST_EDITACTIVITYEPIC:
            return Object.assign({}, state, {
                status: 'editting profile Pic',
                success: action.success,
            })
        case actionType.REQUEST_ACTIVITY:
            return Object.assign({}, state, {
                isLoadingActivity: true,
                success: action.success,
                profile: action.profile,
                isLikedOrDisliked: action.isLikedOrDisliked
            })
        case actionType.REQUEST_INDACTIVITY:
            return Object.assign({}, state, {
                isLoadingIndActivity: true,
                success: action.success,
                profile: action.profile,
                isLikedOrDisliked: action.isLikedOrDisliked
            })
        case actionType.REQUEST_ADDACTIVITY:
            return Object.assign({}, state, {
                status: 'creating activity',
                success: action.success,
                isLoadingActivity: true
            })
        case actionType.REQUEST_EDITACTIVITY:
            return Object.assign({}, state, {
                status: 'editing activity',
                success: action.success,
                activity: action.activity,
                isLoadingActivity: true
            })
        case actionType.REQUEST_DELACTIVITY:
            return Object.assign({}, state, {
                success: action.success,
                isLoadingActivity: true,
                openActivityModal: true,
            })
        case actionType.REQUEST_ADDACTIVITYRATE:
            return Object.assign({}, state, {
                rateSuccess: action.rateSuccess,
                success: true,
                activity: action.activity
            })
        case actionType.REQUEST_ADDACTIVITYCON:
            return Object.assign({}, state, {
                rateSuccess: action.rateSuccess,
                success: true,
                activity: action.activity
            })
        case actionType.LOGIN:
            return Object.assign({}, state, {
                status: 'logged in',
                user: action.user,
                password: action.password,
                success: action.success,
                openLoginModal: false
            })
        case actionType.REGISTER:
            return Object.assign({}, state, {
                status: 'registered',
                user: action.user,
                success: action.success,
                openRegModal: false,
                openLoginModal: true
            })
        case actionType.LOGOUT:
            return Object.assign({}, state, {
                status: 'logged out',
                user: 'guest',
                router: '/'
            })
        case actionType.EDITPROFILE:
            return Object.assign({}, state, {
                status: 'logged in',
                user: action.user,
                profile: action.profile,
                newPassword: action.newPassword,
                oldPassword: action.oldPassword,
                success: action.success,
                openProfileModal: false
            })
        case actionType.EDITPROFILEPIC:
            return Object.assign({}, state, {
                status: 'logged in',
                user: action.user,
                profile: action.profile,
                success: action.success
            })
        case actionType.ACTIVITYPIC:
            return Object.assign({}, state, {
                status: 'logged in',
                success: action.success,
                user: action.user,
                profile: action.profile,
            })
        case actionType.AUTHERROR:
            return Object.assign({}, state, {
                status: 'logging out',
                msg: action.msg,
                passwordVaild: action.passwordVaild,
                openLoginModal: true,
                user: action.user,
            })
        case actionType.REGERROR:
            console.log('from reducer', action.isOpenModel)
            return Object.assign({}, state, {
                status: 'reg error',
                user: action.user,
                msg: action.msg,
                passwordVaild: action.passwordVaild,
                openRegModal: true
            })
        case actionType.RATEERROR:
            return Object.assign({}, state, {
                msg: action.msg,
                user: action.user,
                rateSuccess: action.rateSuccess,
                isLoadingActivity: false
            })
        case actionType.OPENLOGINMODAL:
            return Object.assign({}, state, {
                openLoginModal: true
            })
        case actionType.CLOSELOGINMODAL:
            return Object.assign({}, state, {
                openLoginModal: false
            })
        case actionType.OPENREGMODAL:
            return Object.assign({}, state, {
                openRegModal: true
            })
        case actionType.CLOSEREGMODAL:
            return Object.assign({}, state, {
                openRegModal: false
            })
        case actionType.OPENPROFILEMODAL:
            return Object.assign({}, state, {
                openProfileModal: true
            })
        case actionType.CLOSEPROFILEMODAL:
            return Object.assign({}, state, {
                openProfileModal: false
            })
        case actionType.OPENPROFILEPICMODAL:
            return Object.assign({}, state, {
                openProfilePicModal: true
            })
        case actionType.CLOSEPROFILEPICMODAL:
            return Object.assign({}, state, {
                openProfilePicModal: false
            })
        case actionType.OPENACTIVITYMODAL:
            return Object.assign({}, state, {
                openActivityModal: true
            })
        case actionType.CLOSEACTIVITYMODAL:
            return Object.assign({}, state, {
                openActivityModal: false
            })
        case actionType.OPENPICMODAL:
            return Object.assign({}, state, {
                openPicModal: true
            })
        case actionType.CLOSEPICMODAL:
            return Object.assign({}, state, {
                openPicModal: false
            })
        case actionType.PROFILE:
            return Object.assign({}, state, {
                user: action.user,
                success: action.success,
                email: action.email,
                status: 'logged in',
                profile: action.profile
            })
        case actionType.PROFILEERROR:
            return Object.assign({}, state, {

            })
        case actionType.ACTIVITY:
            return Object.assign({}, state, {
                success: action.success,
                activity: action.activity,
                msg: action.msg,
                isLoadingActivity: false,
                isLikedOrDisliked: action.isLikedOrDisliked,
                openActivityModal: false
            })
        case actionType.INDACTIVITY:
            return Object.assign({}, state, {
                success: action.success,
                activity: action.activity,
                msg: action.msg,
                isLoadingIndActivity: false,
                isLikedOrDisliked: action.isLikedOrDisliked,
                openActivityModal: false
            })
        case actionType.CLEARINDACTIVITY:
            return Object.assign({}, state, {
                isLoadingIndActivity: undefined
            })
        case actionType.ADDACTIVITY:
            return Object.assign({}, state, {
                status: 'logged in',
                success: action.success,
                msg: action.msg,
                openActivityModal: false,
                isLoadingActivity: false,
                activityId: action.activityId,
                activity: action.activity,
            })
        case actionType.EDITACTIVITY:
            return Object.assign({}, state, {
                status: 'logged in',
                success: action.success,
                activity: action.activity,
                msg: action.msg,
                openActivityModal: false,
                isLoadingActivity: false,
            })
        case actionType.DELACTIVITY:
            browserHistory.push('/activity');
            return Object.assign({}, state, {
                success: action.success,
                activity: action.activity,
                msg: action.msg,
                openActivityModal: false,
                isLoadingActivity: false,
            })
        case actionType.ADDACTIVITYRATE:
            return Object.assign({}, state, {
                rateSuccess: action.rateSuccess,
                success: true,
                msg: action.msg,
                isLoadingActivity: false,
                activity: action.activity
            })
        case actionType.ADDACTIVITYCON:
            return Object.assign({}, state, {
                status: 'logged in',
                rateSuccess: action.rateSuccess,
                success: true,
                msg: action.msg,
                isLoadingActivity: false,
                activity: action.activity
            })
        default:
            return state;
    }
}
