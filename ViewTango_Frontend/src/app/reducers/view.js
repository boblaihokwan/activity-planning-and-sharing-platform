/*
*  CSCI3100 - Software engineering Project 2016-2017
*  Project Name: ViewTango 
*  Group: 8 (Diamond 8)
*
*  Filename: view.js
*  Description: To create a reducers on view function.

*  All Rights Reserved.
*/

import { actionType } from '../action/viewActionType'

export default function view(state = {}, action){
    switch(action.type){
        case actionType.RECEIVE_VIEW:
            var update = {};
            update[action.category] = action.data;
            return Object.assign({}, state, update);
        default:
            return state;
    }
}
