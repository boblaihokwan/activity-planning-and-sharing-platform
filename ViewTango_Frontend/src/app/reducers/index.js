/*
*  CSCI3100 - Software engineering Project 2016-2017
*  Project Name: ViewTango 
*  Group: 8 (Diamond 8)
*
*  Filename: index.js
*  Description: Combine multi - Reducers.

*  All Rights Reserved.
*/

import { combineReducers } from 'redux'
import auth from './auth'

export default combineReducers({
  auth
})