/*
*  CSCI3100 - Software engineering Project 2016-2017
*  Project Name: ViewTango 
*  Group: 8 (Diamond 8)
*
*  Filename: profile.js
*  Description: To create a profile reducers on profile function.

*  All Rights Reserved.
*/

import { actionType } from '../action/actionType'

export default function profile(state = { status: 'logged out', username: 'Guest' }, action) {
    switch (action.type) {
        case actionType.REQUEST_PROFILE:
            return Object.assign({}, state, {
                username: action.username,
                success: action.success
            })
        case actionType.PROFILE:
            return Object.assign({}, state, {
                username: action.username,
                success: action.success
            })
        default:
            return state;
    }
}