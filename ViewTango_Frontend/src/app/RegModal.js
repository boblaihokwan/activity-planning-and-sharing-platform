/*
*  CSCI3100 - Software engineering Project 2016-2017
*  Project Name: ViewTango 
*  Group: 8 (Diamond 8)
*
*  Filename: RegModal.js
*  Description: To define a register modal view

*  All Rights Reserved.
*/

import React from 'react';
import { render } from 'react-dom'
import { Button, ButtonToolbar, ButtonGroup, ControlLabel, FormControl, FormGroup, Col, Modal, Glyphicon } from 'react-bootstrap';

var passwordVaild = null;
var genderVaild = 'default';
var passWordMsg = null;
const RegModal = React.createClass({

    handleReg: function () {
        if (this.state.username === "" || this.state.password === "" ||
            this.state.repassword === "" || this.state.gender === null || this.state.email === "" || passwordVaild === 'error') {
            this.setState({
                missingMsg: "Please filling the missing information."
            })
        } else {
            this.setState({
                missingMsg: null
            })
            this.props.actions.register({
                type: 'REGISTER',
                value: this.state
            });
        }
    },

    handleChange: function (event) {

        if (event.target.id === 'username') {
            this.setState({
                username: event.target.value
            })
        } else if (event.target.id === 'password') {
            this.setState({
                password: event.target.value
            })
        } else if (event.target.id === 'repassword') {
            this.setState({
                repassword: event.target.value
            })
        } else if (event.target.id === 'email') {
            this.setState({
                email: event.target.value
            })
        } else if (event.target.id === 'M') {
            this.setState({
                gender: 'M'
            })
        } else if (event.target.id === 'F') {
            this.setState({
                gender: 'F'
            })
        }

        if (this.state.password === this.state.repassword) {
            passwordVaild = null
            passWordMsg = null
        } else {
            passwordVaild = 'error'
            passWordMsg = "Please confirm your password.";
        }
    },

    getInitialState: function () {
        return {
            username: "",
            password: "",
            repassword: "",
            email: "",
            gender: null,
            missingMsg: null
        }
    },

    render() {
        return (
            <Modal {...this.props}>
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-sm">Register</Modal.Title>
                </Modal.Header>
                <Col xs={12} md={12}>
                    <form>
                        <br></br>
                        <ControlLabel>Username:</ControlLabel>
                        <FormControl
                            type="text"
                            id="username"
                            placeholder=""
                            onChange={this.handleChange}
                        />
                        <FormControl.Feedback />
                        <br></br>
                        <ControlLabel>Password:</ControlLabel>
                        <FormGroup validationState={passwordVaild}>
                            <FormControl
                                type="password"
                                id="password"
                                placeholder=""
                                onChange={this.handleChange}
                            />
                            <FormControl.Feedback />
                        </FormGroup>
                        <br></br>
                        <ControlLabel>Re-password:</ControlLabel>
                        <FormGroup validationState={passwordVaild}>
                            <FormControl
                                type="password"
                                id="repassword"
                                placeholder=""
                                onChange={this.handleChange}
                            />
                            <FormControl.Feedback />
                            <ControlLabel>{passWordMsg}</ControlLabel>
                        </FormGroup>
                        <ControlLabel>E-mail:</ControlLabel>
                        <FormControl
                            type="text"
                            id="email"
                            placeholder=""
                            onChange={this.handleChange}
                        />
                        <FormControl.Feedback />
                        <br></br>
                        <ControlLabel>Gender:</ControlLabel>
                        <br></br>
                        <ButtonGroup id="gender">
                            <Button bsStyle={genderVaild} onClick={this.handleChange} id="M" value="M">Male</Button>
                            <Button bsStyle={genderVaild} onClick={this.handleChange} id="F" value="F">Female</Button>
                        </ButtonGroup>
                        <p> </p>
                        <ButtonToolbar>
                            <Button bsStyle="primary" bsSize="large" disabled={this.props.store.status == "reging in"} onClick={this.handleReg}>Submit</Button>
                            <Button type="reset" bsSize="large">Clear</Button>
                        </ButtonToolbar>
                    </form>
                </Col>
                <Modal.Footer>
                    <h7 className="text-danger">{this.state.missingMsg}</h7>
                </Modal.Footer>
            </Modal>
        );
    }
});

export default RegModal