/*
*  CSCI3100 - Software engineering Project 2016-2017
*  Project Name: ViewTango 
*  Group: 8 (Diamond 8)
*
*  Filename: Register.js
*  Description: To define a Register view

*  All Rights Reserved.
*/

import React from 'react';
import ReactDOM from 'react-dom';
import { Button, ButtonToolbar, ControlLabel, FormControl, Col, Modal, ButtonGroup, Glyphicon } from 'react-bootstrap'

export default class Register extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <div>
                <Modal.Dialog>
                    <Col xs={8} md={8}>
                        <form>
                            <br></br>
                            <ControlLabel>Username:</ControlLabel>
                            <FormControl
                                type="text"
                                value={this.state.value}
                                placeholder=""
                                onChange={this.handleChange}
                            />
                            <FormControl.Feedback />
                            <br></br>
                            <ControlLabel>Password:</ControlLabel>
                            <FormControl
                                type="password"
                                value={this.state.value}
                                placeholder=""
                                onChange={this.handleChange}
                            />
                            <FormControl.Feedback />
                            <br></br>
                            <ControlLabel>Re-password:</ControlLabel>
                            <FormControl
                                type="password"
                                value={this.state.value}
                                placeholder=""
                                onChange={this.handleChange}
                            />
                            <FormControl.Feedback />
                            <br></br>
                            <ControlLabel>E-mail:</ControlLabel>
                            <FormControl
                                type="text"
                                value={this.state.value}
                                placeholder=""
                                onChange={this.handleChange}
                            />
                            <FormControl.Feedback />
                            <br></br>
                            <ControlLabel>Gender:</ControlLabel>
                            <br></br>
                            <ButtonGroup>
                                <Button><Glyphicon glyph="star" />Male</Button>
                                <Button><Glyphicon glyph="star" />Female</Button>
                            </ButtonGroup>
                        </form>

                        <ButtonToolbar>
                            <Button bsStyle="primary" bsSize="large">Submit</Button>
                            <Button bsSize="large">Clear</Button>
                        </ButtonToolbar>
                    </Col>
                    <Modal.Footer>
                    </Modal.Footer>
                </Modal.Dialog>
            </div>
        );
    }
}
