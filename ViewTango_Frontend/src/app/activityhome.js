/*
*  CSCI3100 - Software engineering Project 2016-2017
*  Project Name: ViewTango 
*  Group: 8 (Diamond 8)
*
*  Filename: activityhome.js
*  Description: Temp file for the activity function.

*  All Rights Reserved.
*/

import React from 'react';
import { Grid, Row, Col, Panel, Glyphicon } from 'react-bootstrap'


var place = ['Tsim Sha Tsui', 'Sha Tin', 'Tai Po', 'Sai Kung'];
var title = ['Tsim Sha Tsui', 'Sha Tin', 'Tai Po', 'Sai Kung'];

var cards = [];
for (var i = 0; i < 4; i++) {
    cards.push(
        <Col sm={6} md={3}>
            <Panel header={title[i]}>
                <h4><i class="fa fa-fw fa-check"></i> Bootstrap v3.3.7</h4>
            </Panel>
        </Col>
    );
}

const jumpInstance = (
    <div>
        <Grid>
            <Row>
                <Col>
                    <h3><Glyphicon glyph="send"/> Latest Activity</h3>
                </Col>
                {cards}
            </Row>
        </Grid>
    </div>
);

export default class Activityhome extends React.Component {
    render() {
        return (jumpInstance)
    }
}

