/*
*  CSCI3100 - Software engineering Project 2016-2017
*  Project Name: ViewTango 
*  Group: 8 (Diamond 8)
*
*  Filename: store.js
*  Description: To create a store on basic function.

*  All Rights Reserved.
*/

import { createStore, applyMiddleware, combineReducers, subscribe } from 'redux'
import auth from '../reducers/auth'
import view from '../reducers/view'
import thunkMiddleware from 'redux-thunk'
import { routerReducer } from 'react-router-redux'
import { syncHistoryWithStore } from 'react-router-redux'
import { browserHistory } from 'react-router'

const defaultstore = {
    status: 'logged out',
    openLoginModal: false,
    openRegModal: false,
    success: false,
    msg: "",
    user: 'guest',
    email: '',
    passwordVaild: null,
    profile: null,
    activity: null
}

const middleware = applyMiddleware(thunkMiddleware);

var combined = combineReducers({auth,view})

const store = createStore(
    combineReducers({
        combined,
        routing: routerReducer
    }),
    defaultstore,
    middleware
);

export const history = syncHistoryWithStore(browserHistory, store)
export default store;
