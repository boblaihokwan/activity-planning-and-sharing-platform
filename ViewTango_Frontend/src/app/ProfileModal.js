/*
*  CSCI3100 - Software engineering Project 2016-2017
*  Project Name: ViewTango 
*  Group: 8 (Diamond 8)
*
*  Filename: ProfileModal.js
*  Description: To define a profile modal view.

*  All Rights Reserved.
*/

import React from 'react';
import { render } from 'react-dom'
import { Button, ButtonToolbar, ButtonGroup, ControlLabel, FormControl, Form, Col, Row, InputGroup, FormGroup, Modal, Glyphicon, Label } from 'react-bootstrap';
import { connect } from 'react-redux';
import * as action from './action/action';
import { bindActionCreators } from 'redux';

var interest, gear, gearIn, interestIn = [];
var gearedit, interestedit = null;
var passwordVaild = null;
var passWordMsg = null;

const ProfileModal = React.createClass({

    getInitialState: function () {
        var i = 0;
        interest = [];
        interestIn = [];
        for (i = 0; i < this.props.store.profile.interest.length; i++) {
            interest.push(this.props.store.profile.interest[i]);
            interestIn.push(
                <Button bsSize="xsmall" id={this.props.store.profile.interest[i]} onClick={this.handledelInterest}>
                    {this.props.store.profile.interest[i]}
                    <Glyphicon id={this.props.store.profile.interest[i]} glyph="remove" />
                </Button>
            );
        }
        gear = [];
        gearIn = [];
        for (i = 0; i < this.props.store.profile.gear.length; i++) {
            gear.push(this.props.store.profile.gear[i]);
            gearIn.push(
                <Button bsSize="xsmall" id={this.props.store.profile.gear[i]} onClick={this.handledelGear}>
                    {this.props.store.profile.gear[i]}
                    <Glyphicon id={this.props.store.profile.gear[i]} glyph="remove" />
                </Button>
            );
        }
        return {
            password: null,
            missingMsg: null,
            gear: gear,
            gearIn: gearIn,
            gearedit: "",
            interest: interest,
            interestIn: interestIn,
            interestedit: "",
            nickname: this.props.store.profile.nickname,
            description: this.props.store.profile.description,
            user: this.props.store.user,
            oldpassword: this.props.store.password,
            newpassword: "",
            repassword: "",
            profile: this.props.store.profile
        }
    },

    handleEdit: function () {
        if (passwordVaild === 'error') {
            this.setState({
                missingMsg: "Please filling the missing information."
            })
        } else {
            this.setState({
                missingMsg: null
            })
            this.props.actions.editprofile({
                type: 'EDITPROFILE',
                value: {
                    user: this.state.user,
                    oldPassword: this.state.password,
                    newPassword: this.state.newpassword,
                    Profile: {
                        nickname: this.state.nickname,
                        gear: this.state.gear,
                        interest: this.state.interest,
                        description: this.state.description,
                        gender: this.props.store.profile.gender
                    }
                },
            });
        }
    },

    handledelGear: function (event) {
        var index = gear.indexOf(event.target.id);
        if (index >= 0) {
            gear.splice(index, 1);
            gearIn.splice(index, 1);
            this.setState({
                gear: gear,
                gearIn: gearIn
            })
        }
    },

    handledelInterest: function (event) {
        var index = interest.indexOf(event.target.id);
        if (index >= 0) {
            interest.splice(index, 1);
            interestIn.splice(index, 1);
            this.setState({
                interest: interest,
                interestIn: interestIn
            })
        }
    },

    handleAddGear: function () {
        if (gearedit !== null) {
            var newgear = (
                <Button bsSize="xsmall" id={gearedit} onClick={this.handledelGear}>{gearedit}<Glyphicon id={gearedit} glyph="remove" /></Button>
            );
            gear.push(gearedit);
            gearIn.push(newgear);
            this.setState({
                gear: gear,
                gearIn: gearIn,
                gearedit: ""
            })
        }
    },

    handleAddInterest: function () {
        if (interestedit !== null) {
            var newinterest = (
                <Button bsSize="xsmall" id={interestedit} onClick={this.handledelInterest}>{interestedit}<Glyphicon id={interestedit} glyph="remove" /></Button>
            );
            interest.push(interestedit);
            interestIn.push(newinterest);
            this.setState({
                interest: interest,
                interestIn: interestIn,
                interestedit: ""
            })
        }
    },

    handleChange: function (event) {

        if (event.target.id === 'password') {
            this.setState({
                password: event.target.value
            })
        } else if (event.target.id === 'newpassword') {
            this.setState({
                newpassword: event.target.value
            })
        } else if (event.target.id === 'repassword') {
            this.setState({
                repassword: event.target.value
            })
        } else if (event.target.id === 'email') {
            this.setState({
                email: event.target.value
            })
        } else if (event.target.id === 'nickname') {
            this.setState({
                nickname: event.target.value
            })
        } else if (event.target.id === 'description') {
            this.setState({
                description: event.target.value
            })
        } else if (event.target.id === 'gear') {
            this.setState({
                gearedit: event.target.value
            })
            gearedit = event.target.value
        } else if (event.target.id === 'interest') {
            this.setState({
                interestedit: event.target.value
            })
            interestedit = event.target.value
        }

        if (this.state.password) {
            if (this.state.password === this.state.oldpassword) {
                if (this.state.newpassword === this.state.repassword) {
                    passwordVaild = null
                    passWordMsg = null
                } else {
                    passwordVaild = 'error'
                    passWordMsg = "Please confirm your password.";

                }
            } else {
                passwordVaild = 'error'
                passWordMsg = "Please confirm your password.";
            }
        } else {
            passwordVaild = null
            passWordMsg = null
        }
    },

    render() {
        return (
            <Modal {...this.props } >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-sm"><Glyphicon glyph="user" /> Edit User</Modal.Title>
                </Modal.Header>
                <Col xs={12} md={12}>
                    <Form horizontal>
                        <br></br>
                        <FormGroup controlId="user">
                            <Col componentClass={ControlLabel} sm={2}>User</Col>
                            <Col sm={10}>
                                <FormControl type="text" placeholder="User" value={this.state.user} disabled />
                            </Col>
                        </FormGroup>
                        <FormGroup controlId="password" validationState={passwordVaild}>
                            <Col componentClass={ControlLabel} sm={2}>Current-Password</Col>
                            <Col sm={10}>
                                <FormControl id="password" type="password" placeholder="Type your old password" onChange={this.handleChange} />
                            </Col>
                        </FormGroup>
                        <FormGroup controlId="newpassword" validationState={passwordVaild}>
                            <Col componentClass={ControlLabel} sm={2}>New-Password</Col>
                            <Col sm={10}>
                                <FormControl id="newpassword" type="password" placeholder="Type your new password" onChange={this.handleChange} />
                            </Col>
                        </FormGroup>
                        <FormGroup controlId="repassword" validationState={passwordVaild}>
                            <Col componentClass={ControlLabel} sm={2}>Re-Password</Col>
                            <Col sm={10}>
                                <FormControl id="repassword" type="text" placeholder="Type your password again" onChange={this.handleChange} />
                            </Col>
                        </FormGroup>
                        <FormGroup controlId="nickname">
                            <Col componentClass={ControlLabel} sm={2}>Nickname</Col>
                            <Col sm={10}>
                                <FormControl id="nickname" type="text" placeholder="Nickname" value={this.state.nickname} onChange={this.handleChange} />
                            </Col>
                        </FormGroup>
                        <FormGroup controlId="email">
                            <Col componentClass={ControlLabel} sm={2}>Email</Col>
                            <Col sm={10}>
                                <FormControl id="email" type="text" placeholder="email" value={this.props.store.email} onChange={this.handleChange} disabled />
                            </Col>
                        </FormGroup>
                        <FormGroup controlId="description">
                            <Col componentClass={ControlLabel} sm={2}>Description</Col>
                            <Col sm={10}>
                                <FormControl id="description" type="text" placeholder="Description" value={this.state.description} onChange={this.handleChange} />
                            </Col>
                        </FormGroup>
                        <FormGroup>
                            <Col componentClass={ControlLabel} sm={2}>Gear</Col>
                            <Col sm={10}>
                                <p>{this.state.gearIn}</p>
                                <InputGroup>
                                    <FormControl id="gear" type="text" placeholder="Add your gear" value={this.state.gearedit} onChange={this.handleChange} />
                                    <InputGroup.Button>
                                        <Button onClick={this.handleAddGear}><Glyphicon glyph="plus" /></Button>
                                    </InputGroup.Button>
                                </InputGroup>
                            </Col>
                        </FormGroup>
                        <FormGroup controlId="interest">
                            <Col componentClass={ControlLabel} sm={2}>Interest</Col>
                            <Col sm={10}>
                                <p>{this.state.interestIn}</p>
                                <InputGroup>
                                    <FormControl id="interest" type="text" placeholder="Add your Interest" value={this.state.interestedit} onChange={this.handleChange} />
                                    <InputGroup.Button>
                                        <Button onClick={this.handleAddInterest}><Glyphicon glyph="plus" /></Button>
                                    </InputGroup.Button>
                                </InputGroup>
                            </Col>
                        </FormGroup>
                        <p></p>
                        <ButtonToolbar>
                            <Button bsStyle="primary" bsSize="large" typr="submit" disabled={this.props.store.status == "editing profile"} onClick={this.handleEdit}>Save</Button>
                            <Button type="reset" bsSize="large">Clear</Button>
                        </ButtonToolbar>
                    </Form>
                </Col>
                <Modal.Footer>
                    <h7 className="text-danger">{this.state.missingMsg === null ? this.props.store.msg : this.state.missingMsg}</h7>
                </Modal.Footer>
            </Modal >
        );
    }
});

export default ProfileModal