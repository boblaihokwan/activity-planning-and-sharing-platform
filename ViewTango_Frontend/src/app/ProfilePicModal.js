/*
*  CSCI3100 - Software engineering Project 2016-2017
*  Project Name: ViewTango 
*  Group: 8 (Diamond 8)
*
*  Filename: ProfilePicModal.js
*  Description: To define a profile picture modal view

*  All Rights Reserved.
*/

import React from 'react';
import { render } from 'react-dom'
import { Button, ButtonToolbar, ButtonGroup, ControlLabel, FormControl, Col, Modal, Glyphicon } from 'react-bootstrap';
import Dropzone from 'react-dropzone';
import '../css/custom.css'
import { apiPath } from './apiPath'

//Define the path of API
var propicapipath = apiPath.propic
var activitypath = apiPath.activityPic
var dzstyle = {
    width: '100%',
    height: '100%',
    textAlign: 'center',
    border: '3px dashed rgb(245, 244, 243)'
}

const ProfilePicModal = React.createClass({

    getInitialState: function () {
        if (this.props.act === "PROFILE") {
            if (this.props.store.profile.propic) {
                return {
                    filepath: propicapipath + this.props.store.profile.propic,
                    propic: this.props.store.profile.propic,
                    user: "",
                    password: "",
                    missingMsg: null,
                    file: [],
                    editingId: null,
                    function: "Upload your Profile Picture"
                }
            } else {
                return {
                    user: "",
                    password: "",
                    missingMsg: null,
                    file: [],
                    filepath: null,
                    editingId: null
                }
            }
        } else if (this.props.act === "ACTIVITY") {
            if (this.props.store.activity.contents[this.props.id].imageUrl) {
                return {
                    filepath: activitypath + this.props.store.activity.contents[this.props.id].imageUrl,
                    propic: this.props.store.activity.contents[this.props.id].imageUrl,
                    missingMsg: null,
                    file: [],
                    editingId: null,
                    function: "Upload your Activity Picture"
                }
            } else {
                return {
                    file: [],
                    filepath: null,
                    propic: null,
                    editingId: null
                }
            }
        } else {
            return {
                user: "",
                password: "",
                missingMsg: null,
                file: [],
                filepath: null,
                propic: null,
                editingId: null
            }
        }
    },

    onHandleUpload: function (res) {
        this.setState({
            file: res,
        })
        if (this.props.act === "PROFILE") {
            this.props.actions.editProfilePic({
                type: 'Profile',
                media: {
                    media: res,
                    Profile: this.props.store.profile,
                    user: this.props.store.user
                }
            });
        } else if (this.props.act === "ACTIVITY") {
            this.props.actions.editActivityPic({
                type: 'Activity',
                eventID: this.props.store.activity._id,
                user: this.props.store.user,
                contentID: this.props.store.activity.contents[this.props.id]._id,
                media: {
                    media: res,
                }
            });
        }
    },

    render() {
        if (this.props.act === "ACTIVITY") {
            if (this.props.store.activity.contents[this.props.id].imageUrl) {
                this.state = {
                    filepath: activitypath + this.props.store.activity.contents[this.props.id].imageUrl,
                    propic: this.props.store.activity.contents[this.props.id].imageUrl,
                    missingMsg: null,
                    file: [],
                    editingId: null,
                    function: "Upload your Activity Picture"
                }
            } else {
                this.state = {
                    filepath: null,
                    propic: null,
                    missingMsg: null,
                    file: [],
                    editingId: null
                }
            }
        }
        return (
            <Modal {...this.props}>
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-sm"><Glyphicon glyph="send" />{" "}{this.state.function}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Dropzone style={dzstyle} onDrop={this.onHandleUpload.bind(this)} multiple="false" accept="image/jpeg">
                        {this.state.filepath ?
                            <img height="200" src={this.state.filepath} />
                            : null
                        }
                        <div>
                            <h1><Glyphicon glyph="picture" /></h1>
                            <p>Dropping a Picture here, or click to select a Picture to upload.</p>
                        </div>
                    </Dropzone>
                </Modal.Body>
                <Modal.Footer>
                    <h7 className="text-danger">{this.state.missingMsg === null ? this.props.store.msg : this.state.missingMsg}</h7>
                </Modal.Footer>
            </Modal>
        );
    }
});

export default ProfilePicModal