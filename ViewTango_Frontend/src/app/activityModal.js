/*
*  CSCI3100 - Software engineering Project 2016-2017
*  Project Name: ViewTango 
*  Group: 8 (Diamond 8)
*
*  Filename: activityModal.js
*  Description: To define a add activity modal view.

*  All Rights Reserved.
*/

import React from 'react';
import { render } from 'react-dom'
import { Button, ButtonToolbar, ControlLabel, FormControl, Form, Col, InputGroup, FormGroup, Modal, Glyphicon, Panel } from 'react-bootstrap'
import { browserHistory } from 'react-router'
import moment from 'moment'
import Autocomplete from 'react-google-autocomplete';
import '../css/custom.css'

var geocoder = new google.maps.Geocoder();
var map, mapOptions, marker;
var marker, infoWindow, checkForMarkerClick = 0;
var eventIn, timelines = [];
var eventedit, timeedit, eventdescriptionedit;

const ActivityModal = React.createClass({

    getInitialState: function () {
        if (this.props.id === "POST") {
            timelines = [];
            eventIn = [];
            return {
                creator: this.props.store.user,
                title: null,
                description: null,
                activity: null,
                eventIn: null,
                eventedit: null,
                eventdescriptionedit: null,
                timeedit: null,
                timelines: null,
                time: null,
                date: null,
                delbutton: "",
                function: "Add Activity"
            }
        } else if (this.props.id === "PUT") {
            this.handleLoadEvent();
            return {
                activity: this.props.store.activity,
                creator: this.props.store.user,
                title: this.props.store.activity.title,
                description: this.props.store.activity.description,
                activity: this.props.store.activity,
                eventIn: eventIn,
                eventedit: null,
                eventdescriptionedit: null,
                timeedit: null,
                timelines: this.props.store.activity.contents,
                time: this.props.store.activity.time,
                date: moment(this.props.store.activity.date).format('YYYY-MM-DD'),
                location: this.props.store.activity.location,
                pplneed: this.props.store.activity.peopleneed,
                categories: this.props.store.activity.categories,
                id: this.props.store.activity._id,
                delbutton: <Button bsStyle="danger" bsSize="large" onClick={this.handleDelActivity}>Delete</Button>,
                function: "Edit Activity"
            }
        }
    },

    handleAddActivity: function () {
        var currentdate = new Date();
        var datetime = currentdate.getFullYear() + " - "
            + (currentdate.getMonth() + 1) + "-"
            + currentdate.getDate() + "-" + "T"
            + currentdate.getHours() + ":"
            + currentdate.getMinutes() + ":"
            + currentdate.getSeconds();
        if (this.state.creator === "" || this.state.title === "" ||
            this.state.date === null || categories === null || this.state.peopleneed === null || this.state.timelines === null) {
            this.setState({
                missingMsg: "Please filling the missing information."
            })
        } else if (this.props.id === "POST") {
            this.props.actions.addactivity({
                type: 'ACTIVITY',
                value: {
                    author: {
                        username: this.state.creator,
                    },
                    contents: this.state.timelines,
                    title: this.state.title,
                    date: this.state.date,
                    location: this.state.location,
                    categories: [this.state.categories],
                    peopleneed: this.state.pplneed,
                    description: this.state.description
                }
            });
        } else if (this.props.id === "PUT") {
            this.props.actions.editactivity({
                type: 'ACTIVITY',
                activity: this.state.activity,
                value: {
                    path: '/' + this.state.id,
                    _id: this.state.id,
                    author: {
                        username: this.state.creator,
                    },
                    contents: this.state.timelines,
                    title: this.state.title,
                    date: this.state.date,
                    location: this.state.location,
                    categories: [this.state.categories],
                    peopleneed: this.state.pplneed,
                    description: this.state.description
                }
            });
        }
    },

    handleAddEvent: function () {
        if (eventedit !== null && timeedit !== null && timeedit !== undefined &&
            eventedit !== undefined && eventdescriptionedit !== null && 
            eventdescriptionedit !== undefined) {
            var newevent = (
                <p>
                    <Form inline>
                        <FormControl id="time" type="time" value={timeedit} disabled />{" "}
                        <InputGroup>
                            <FormControl id="event" type="text" value={eventedit} disabled />
                            <InputGroup.Button>
                                <Button onClick={this.handleDelEvent} id={timeedit}><Glyphicon id={timeedit} glyph="remove" /></Button>
                            </InputGroup.Button>
                        </InputGroup>
                        <p>
                            <FormControl componentClass="textarea" id={eventdescription} type="text" value={eventdescriptionedit} disabled />
                        </p>
                    </Form>
                </p>
            );

            var event = {
                time: timeedit,
                title: eventedit,
                description: eventdescriptionedit
            }
            timelines.push(event);
            eventIn.push(newevent);
            this.setState({
                eventIn: eventIn,
                eventedit: "",
                timeedit: "",
                eventdescriptionedit: "",
                timelines: timelines,
                missingMsg: null
            })
        }else{
            this.setState({
                missingMsg: "Please confirm the event information."
            })
        }
    },

    handleDelActivity: function (id) {
        this.props.actions.delactivity({
            type: 'ACTIVITY',
            value: {
                path: '/' + this.state.id,
                _id: this.state.id
            }
        });
        browserHistory.push('/activity');
    },

    handleDelEvent: function (event) {
        var i = this.state.timelines.length;
        timelines = this.state.timelines
        while (i--) {
            if (timelines[i]
                && timelines[i].hasOwnProperty("time")
                && (arguments.length >= 2 && timelines[i]["time"] === event.target.id)) {
                timelines.splice(i, 1);
                eventIn.splice(i, 1);
                this.setState({
                    timelines: timelines,
                    eventIn: eventIn
                })
            }
        }
    },

    handleLoadEvent() {
        eventIn = [];
        timelines = [];
        for (var i = 0; i < this.props.store.activity.contents.length; i++) {
            var newevent = (
                <p>
                    <Form inline>
                        <FormControl id={this.props.store.activity.contents[i].time} type="time" value={this.props.store.activity.contents[i].time} disabled />{" "}
                        <InputGroup>
                            <FormControl id="event" type="text" value={this.props.store.activity.contents[i].title} disabled />
                            <InputGroup.Button>
                                <Button onClick={this.handleDelEvent} id={this.props.store.activity.contents[i].time}><Glyphicon id={this.props.store.activity.contents[i].time} glyph="remove" /></Button>
                            </InputGroup.Button>
                        </InputGroup>
                        <p>
                            <FormControl componentClass="textarea" type="text" value={this.props.store.activity.contents[i].description} disabled />
                        </p>
                    </Form>
                </p>
            )
            eventIn.push(newevent);
        }
        timelines = this.props.store.activity.contents;
    },

    handleChange: function (event) {
        if (event.target.id === 'creator') {
            this.setState({
                creator: event.target.value
            })
        } else if (event.target.id === 'title') {
            this.setState({
                title: event.target.value
            })
        } else if (event.target.id === 'date') {
            this.setState({
                date: event.target.value
            })
        } else if (event.target.id === 'location') {
            this.setState({
                location: event.target.value
            })
        } else if (event.target.id === 'pplneed') {
            this.setState({
                pplneed: event.target.value
            })
        } else if (event.target.id === 'description') {
            this.setState({
                description: event.target.value
            })
        } else if (event.target.id === 'categories') {
            this.setState({
                categories: event.target.value
            })
        } else if (event.target.id === 'event') {
            this.setState({
                eventedit: event.target.value
            })
            eventedit = event.target.value
        } else if (event.target.id === 'time') {
            this.setState({
                timeedit: event.target.value
            })
            timeedit = event.target.value
        } else if (event.target.id === 'eventdescription') {
            this.setState({
                eventdescriptionedit: event.target.value
            })
            eventdescriptionedit = event.target.value
        }
    },

    render() {
        return (
            <Modal {...this.props } >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-sm"><Glyphicon glyph="camera" />{" "}{this.state.function}</Modal.Title>
                </Modal.Header>
                <Col xs={12} md={12}>
                    <Form horizontal>
                        <br></br>
                    
                        <FormGroup>
                            <Col componentClass={ControlLabel} sm={3}>Title</Col>
                            <Col sm={9}>
                                <FormControl id="title" type="text" placeholder="Title" value={this.state.title} onChange={this.handleChange} />
                            </Col>
                        </FormGroup>
                        <FormGroup>
                            <Col componentClass={ControlLabel} sm={3}>Date</Col>
                            <Col sm={9}>
                                <FormControl id="date" type="date" value={this.state.date} onChange={this.handleChange} />
                            </Col>
                        </FormGroup>
                        <FormGroup>
                        <Col componentClass={ControlLabel} sm={3}>Location</Col>
                        <Col sm={9}>
                            <Autocomplete
                            className="form-control"
                                id="location"
                                style={{ width: '100%' }}
                                value={this.state.location}
                                onChange={this.handleChange}
                                onPlaceSelected={(place) => {
                                    this.setState({
                                        location: place.formatted_address
                                    })
                                }}
                                types={['establishment']}
                                componentRestrictions={{ country: "hk" }}
                            />
                        </Col>
                        </FormGroup>
                        <FormGroup>
                            <Col componentClass={ControlLabel} sm={3}>People Needed</Col>
                            <Col sm={9}>
                                <FormControl id="pplneed" type="number" placeholder="Enter the people need" value={this.state.pplneed} onChange={this.handleChange} />
                            </Col>
                        </FormGroup>
                        <FormGroup>
                            <Col componentClass={ControlLabel} sm={3}>Description</Col>
                            <Col sm={9}>
                                <FormControl componentClass="textarea" id="description" type="text" placeholder="Description" value={this.state.description} onChange={this.handleChange} />
                            </Col>
                        </FormGroup>
                        <FormGroup>
                            <Col componentClass={ControlLabel} sm={3}>Category</Col>
                            <Col sm={9}>
                                <FormControl componentClass="select" placeholder="select" id="categories" type="text" value={this.state.categories} onChange={this.handleChange}>
                                    <option value="City View">City View</option>
                                    <option value="Venues">Venues</option>
                                    <option value="Sunrise / Sunset">Sunrise / Sunset</option>
                                    <option value="Nightscape">Nightscape</option>
                                    <option value="Festival">Festival</option>
                                </FormControl>
                            </Col>
                        </FormGroup>
                        <FormGroup>
                            <Col componentClass={ControlLabel} sm={3}>Timeline</Col>
                            <Panel>
                                <Col sm={9}>
                                    {this.state.eventIn}
                                    <Form inline>
                                        <FormControl id="time" type="time" value={this.state.timeedit} onChange={this.handleChange} />{" "}
                                        <InputGroup>
                                            <FormControl id="event" type="text" placeholder="Add your Activity" value={this.state.eventedit} onChange={this.handleChange} />
                                            <InputGroup.Button>
                                                <Button onClick={this.handleAddEvent}><Glyphicon glyph="plus" /></Button>
                                            </InputGroup.Button>
                                        </InputGroup>
                                    </Form>
                                    <p>
                                        <FormControl componentClass="textarea" id="eventdescription" type="text" placeholder="Description" value={this.state.eventdescriptionedit} onChange={this.handleChange} />
                                    </p>
                                </Col>
                            </Panel>
                        </FormGroup>
                        <p></p>
                        <ButtonToolbar>
                            <Button disabled={this.props.store.status == 'editing activity' || this.props.store.status == 'creating activity'} bsStyle="primary" bsSize="large" typr="submit" onClick={this.handleAddActivity}>Save</Button>
                            <Button type="reset" bsSize="large">Clear</Button>
                            {this.state.delbutton}
                        </ButtonToolbar>
                    </Form>
                </Col>
                <Modal.Footer>
                    <h7 className="text-danger">{this.state.missingMsg === null ? this.props.store.msg : this.state.missingMsg}</h7>
                </Modal.Footer>
            </Modal >
        );
    }
})

export default ActivityModal

