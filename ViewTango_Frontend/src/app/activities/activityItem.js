/*
*  CSCI3100 - Software engineering Project 2016-2017
*  Project Name: ViewTango 
*  Group: 8 (Diamond 8)
*
*  Filename: activityItem.js
*  Description: To list the activity items.

*  All Rights Reserved.
*/

import React from 'react';
import ReactDOM from 'react-dom';
import { Grid, Panel, Media, img, handleClick, Button, Nav, Glyphicon, Label, Modal, Table, Image } from 'react-bootstrap'
import TimeAgo from 'react-timeago'
import { Link, browserHistory, history, createHistory } from 'react-router'
import { apiPath } from '../apiPath'
import Moment from 'react-moment'

//Define the path of API
var activitypath = apiPath.activityPic
var staticgmap = apiPath.staticGMap

var place = [];
var landHead = [];
var land = [];
var event = [];

const Items = React.createClass({

    getInitialState: function () {
        var i = 0;
        var loadingmodal = (
            <Modal.Dialog >
                <Modal.Body><h4>It's' just loading.</h4></Modal.Body>
            </Modal.Dialog >
        );
        if (!this.props.store.activity) {
            return {
                loadingActivity: loadingmodal
            }
        } else {
            return {
                loadingActivity: ''
            }
        }
    },

    handleActivity: function (event) {
        this.props.actions.activity({
            type: 'ACTIVITY',
            value: '/' + event.target.id
        });
    },

    getActivity() {
        var i = 0;
        event = [];
        var space = "&nbsp;";
        console.log(this.props.store.activity);

        //handle rate
        for (i = 0; i < this.props.store.activity.length; i++) {
            var total, likeAvg = 0;
            var starIn = [];
            total = this.props.store.activity[i].likes + this.props.store.activity[i].dislikes;
            likeAvg = (this.props.store.activity[i].likes / total) * 5;
            for (var j = 0; j < 5; j++) {
                if (j <= likeAvg) {
                    starIn.push(<Glyphicon glyph="star" />);
                } else {
                    starIn.push(<Glyphicon glyph="star-empty" />);
                }
            }

            //handle photo
            var picturePath = null;
            for (var j = 0; j < this.props.store.activity[i].contents.length; j++) {
                if (this.props.store.activity[i].contents[j].imageUrl !== "") {
                    picturePath = activitypath + this.props.store.activity[i].contents[j].imageUrl;
                    break;
                } else {
                    picturePath = staticgmap + "&zoom=16&size=242x200&center=" + this.props.store.activity[i].location;
                }
            }

            event.push(
                <div className="panel panel-default">
                    <div className="panel-body">
                        <Media>
                            <Media.Left align="top">
                                <Image width={242} height={200} src={picturePath} rounded />
                            </Media.Left>
                            <Media.Body>
                                <Panel>
                                    <Media.Heading>
                                        <a>{this.props.store.activity[i].title}</a>
                                    </Media.Heading>
                                    <Table>
                                        <tbody>
                                            <tr>
                                                <td className="col-md-4"><Label bsStyle="success">Date</Label>{" "}<Moment format="YYYY/MM/DD">{this.props.store.activity[i].date}</Moment></td>
                                                <td className="col-md-4"><Label bsStyle="primary">Catgory</Label>{" "}{this.props.store.activity[i].categories}</td>
                                            </tr>
                                            <tr>
                                                <td className="col-md-4"><Label bsStyle="warning">Place</Label>{" "}{this.props.store.activity[i].location}</td>
                                                <td className="col-md-4"><Label bsStyle="danger">Rating</Label>{" "}{starIn}</td>
                                                <td className="col-md-4"><Label bsStyle="info">People need</Label>{" "}<Glyphicon glyph="user" />{this.props.store.activity[i].peopleneed}</td>
                                            </tr>
                                        </tbody>
                                    </Table>
                                </Panel>
                                {this.props.store.activity[i].description}
                            </Media.Body>
                        </Media>
                        <Link to={'/activity/' + this.props.store.activity[i]._id}><Button id={this.props.store.activity[i]._id} className="pull-right" bsStyle="warning" ><Glyphicon id={this.props.store.activity[i]._id} glyph="flag" />  View</Button></Link>
                    </div>
                    <div className="panel-footer">
                        <div className="pull-left"><Glyphicon glyph="user" />{" "}{this.props.store.activity[i].author.username} </div>
                        <div className="pull-right"><Glyphicon glyph="time" />{" "}<TimeAgo date={this.props.store.activity[i].createdAt} /></div>
                        <div className="clearfix"></div>
                    </div>
                </div>
            );
        }
    },

    render() {
        this.getActivity();
        return (
            <div style={{ width: "97%", margin: "0 auto" }}>
                <br />
                {event}
                <br />
            </div>
        );
    }
})

export default Items