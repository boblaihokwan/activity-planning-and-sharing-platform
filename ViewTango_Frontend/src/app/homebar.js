/*
*  CSCI3100 - Software engineering Project 2016-2017
*  Project Name: ViewTango 
*  Group: 8 (Diamond 8)
*
*  Filename: homebar.js
*  Description: To define the application nav-bar.

*  All Rights Reserved.
*/

import React from 'react';
import { render } from 'react-dom'
import { Navbar, Nav, NavItem, NavDropdown, MenuItem, Button, ButtonToolbar, ButtonGroupCol, Glyphicon, Label } from 'react-bootstrap';
import { Link, browserHistory, history, createHistory, IndexLink } from 'react-router'
import { bootstrapUtils } from 'react-bootstrap/lib/utils';
import LoginModal from './LoginModal'
import RegModal from './RegModal'
import Profile from './profile'

bootstrapUtils.addStyle(Navbar, 'custom');

var user;

const LogoutButton = React.createClass({

    handleLogout: function () {
        user = null;
        this.props.actions.logout({
            type: 'LOGOUT',
            user: 'guest'
        });
        browserHistory.push('/');
    },

    render() {
        return (
            <Button bsStyle="warning" onClick={this.handleLogout} >Logout</Button>
        );
    }
});

const ProfileButton = React.createClass({

    handleProfile: function () {
        this.props.actions.profile({
            type: 'PROFILE',
            value: this.props.store.user
        });
    },
    render() {
        return (
            <Link to={'profile'}><Button bsStyle="primary" onClick={this.handleProfile}><Glyphicon glyph="user" /> {user} </Button></Link>
        );
    }
});

const ActivityButton = React.createClass({
    render() {
        return (
            <NavItem><Link to={'/activity'}>Activity</Link></NavItem>
        );
    }
});

class Homebar extends React.Component {

    constructor() {
        super();
        this.state = {
            loginShow: false,
            registerShow: false
        }
    }

    componentWillMount() {
        this.props.actions.profile({
            type: 'PROFILE',
            value: this.props.store.user,
            close: true
        });
    }

    render() {
        var loginstates;

        if (this.props.store.status === 'logged in') {
            if (!user || user === null) user = this.props.store.user;
            loginstates = (
                <ButtonToolbar>
                    <ProfileButton bsStyle="warning" onClick={this.props.actions.profile} actions={this.props.actions} store={this.props.store} />
                    <LogoutButton bsStyle="warning" onClick={this.props.actions.logout} disabled={this.props.store.status == "logging out"} actions={this.props.actions} store={this.props.store} />
                </ButtonToolbar>
            )
        } else {
            user = null;
            loginstates = (
                <ButtonToolbar>
                    <Button bsStyle="info" onClick={() => this.props.actions.openRegModal()}>Register</Button>
                    <Button bsStyle="warning" onClick={() => this.props.actions.openLoginModal()}>Login</Button>
                </ButtonToolbar>
            )
        }
        return (
            <div>
                <Navbar bsStyle=''>
                    <Nav pullRight>
                        <ButtonToolbar>

                        </ButtonToolbar>
                    </Nav>
                </Navbar>
                <div class="container">
                    <Navbar collapseOnSelect fixedTop='true'>
                        <Navbar.Header bsStyle='default'>
                            <Navbar.Brand>
                                <IndexLink to={'/'}>
                                    <img width="40px" src={"http://viewtango-testdatabase.rhcloud.com/src/assets/" + "favicon.ico"} />
                                </IndexLink>
                            </Navbar.Brand>
                            <Navbar.Toggle />
                        </Navbar.Header>
                        <Navbar.Collapse>
                            <Nav>
                                <NavItem href="#"><Link to={'/view'}>View Explorer</Link></NavItem>
                                <ActivityButton onClick={this.props.actions.activity} actions={this.props.actions} store={this.props.store} />
                            </Nav>
                            <Nav pullRight>
                                <NavItem>
                                    {loginstates}
                                </NavItem>
                            </Nav>
                        </Navbar.Collapse>
                    </Navbar>
                </div>
                <LoginModal show={this.props.store.openLoginModal} onHide={this.props.actions.closeLoginModal} actions={this.props.actions} store={this.props.store} />
                <RegModal show={this.props.store.openRegModal} onHide={this.props.actions.closeRegModal} actions={this.props.actions} store={this.props.store} />
            </div>
        );
    }
}
export default Homebar
