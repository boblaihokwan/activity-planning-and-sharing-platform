/*
*  CSCI3100 - Software engineering Project 2016-2017
*  Project Name: ViewTango 
*  Group: 8 (Diamond 8)
*
*  Filename: categoryItem.js
*  Description: To list the category item in view function.

*  All Rights Reserved.
*/

import React from 'react';
import ReactDOM from 'react-dom';
import { Grid, Col, Row, Thumbnail, Carousel, Button, Image, Panel, container, Media, Glyphicon, Modal } from 'react-bootstrap'
import { apiPath } from '../apiPath'
import picModal from './picModal'
import * as action from '../action/action';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

var imgPrefix = apiPath.activityPic;

class CategoryItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showModal: false
        };

        this.open = this.open.bind(this);
        this.close = this.close.bind(this);
    }

    close() {
        this.setState({
            showModal: false
        });
    }

    open(event) {
        this.setState({
            showModal: true,
            picuser: event.target.id,
            openingsrc: event.target.src,
            title: event.target.alt
        });
    }

    render() {
        var place = [];
        if (this.props.store && this.props.store[this.props.value]) {
            var content = this.props.store[this.props.value];

            for (var i = 0; i < content.length; i++) {
                var contents = content[i].contents
                for (var j = 0; j < contents.length; j++) {
                    if (!contents[j].imageUrl || contents[j].imageUrl == 0)
                        continue;
                    var realimg = imgPrefix + contents[j].imageUrl;
                    place.push(
                        <Col xs={12} md={3}>
                            <Panel >
                                <Media>
                                    <Media.Body>
                                        <Media.Heading className="view-thumbnail">
                                            <img id={content[i].author.username} alt={content[i].title} onClick={this.open} src={realimg} width="100%" className="center-crop" />
                                        </Media.Heading>
                                        <a><h5>{content[i].title}</h5></a>
                                        <p><small><Glyphicon glyph="user" />{" "}{content[i].author.username}</small></p>
                                    </Media.Body>
                                </Media>
                            </Panel>
                        </Col>
                    )
                }
            }
        }

        return (
            <container-fluid marginTop="30px" className="tab-content">
                <Row className='.text-center'>
                    {place}
                    <Modal show={this.state.showModal} onHide={this.close} {...this.props}>
                        <Modal.Body>
                            <img src={this.state.openingsrc} width="100%" className="center-crop" />
                        </Modal.Body>
                        <Modal.Footer>
                            <center><h4>{this.state.title}</h4>{" "}<small>{this.state.picuser}</small></center>
                        </Modal.Footer>
                    </Modal>
                </Row>
            </container-fluid>
        );
    }
}
export default CategoryItem