/*
*  CSCI3100 - Software engineering Project 2016-2017
*  Project Name: ViewTango 
*  Group: 8 (Diamond 8)
*
*  Filename: festival.js
*  Description: To list the festival view on view function.

*  All Rights Reserved.
*/

import React from 'react';
import ReactDOM from 'react-dom';
import { Grid, Col, Row, Thumbnail, Carousel } from 'react-bootstrap'

var place = [];
for (var i = 0; i < 2; i++) {
    place.push(
        <Col xs={6} md={4}>
            <Thumbnail src="src/assets/test.png" alt="242x200">
                <Carousel.Caption>
                    <h3>Thumbnail label</h3>
                </Carousel.Caption>
            </Thumbnail>
        </Col>
    )
}

export default class Festival extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <div>
                <p>Here is festival.js</p>
                <Grid>
                    <Row>
                        {place}
                    </Row>
                </Grid>
            </div>
        );
    }
}
