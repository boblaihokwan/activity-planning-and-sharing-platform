/*
*  CSCI3100 - Software engineering Project 2016-2017
*  Project Name: ViewTango 
*  Group: 8 (Diamond 8)
*
*  Filename: picModal.js
*  Description: To define a picture modal view.

*  All Rights Reserved.
*/

import React from 'react';
import { render } from 'react-dom'
import { Button, ButtonToolbar, ButtonGroup, ControlLabel, FormControl, Col, Modal, Glyphicon } from 'react-bootstrap';

const picModal = React.createClass({

    getInitialState: function () {
        return {

        }
    },

    render() {
        return (
            <Modal {...this.props}>
                <Modal.Body>
                    One fine body...
                </Modal.Body>
                <Modal.Footer>
                </Modal.Footer>
            </Modal>
        );
    }
});

export default picModal