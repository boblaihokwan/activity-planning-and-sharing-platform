/*
*  CSCI3100 - Software engineering Project 2016-2017
*  Project Name: ViewTango 
*  Group: 8 (Diamond 8)
*
*  Filename: viewAction.js
*  Description: To define the action of the view picture function.

*  All Rights Reserved.
*/

import { actionType } from './viewActionType'
import fetch from 'isomorphic-fetch'
import { apiPath } from '../apiPath'

var url_category = apiPath.category;

function requestView() {
    return {
        type: actionType.REQUEST_VIEW
    }
}

function receiveView(category, json) {
    if (json.success) {
        return {
            type: actionType.RECEIVE_VIEW,
            data: json.data,
            category: category
        }
    }
}

export function getView(category) {
    return function (dispatch) {
        dispatch(requestView);
        var url = url_category + '/viewList?category=' + category;


        return fetch(url, {
            method: 'GET',
            credentials: 'include',
            headers: {
                'Content-type': 'application/json'
            }
        }).then(response => response.json())
            .then(json => {
                //if login success
                if (json.success == true && json !== undefined) {
                    dispatch(receiveView(category, json))
                } else {
                    //dispatch(regError(json))
                }
            })
    }
}
