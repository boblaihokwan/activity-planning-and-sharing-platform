/*
*  CSCI3100 - Software engineering Project 2016-2017
*  Project Name: ViewTango 
*  Group: 8 (Diamond 8)
*
*  Filename: action.js
*  Description: To define the action on the application.

*  All Rights Reserved.
*/

//Define the file and library
import { actionType } from './actionType'
import fetch from 'isomorphic-fetch'
import { Link, browserHistory, history, createHistory } from 'react-router'
import { apiPath } from '../apiPath'

//Define the path of API
var Apipath_auth = apiPath.auth
var Apipath_user = apiPath.user
var Apipath_activity = apiPath.activity

export const REQUEST_REG = 'REQUEST_REG'
function requestReg(subreddit) {
  return {
    type: actionType.REQUEST_REG
  }
}

function requestAddActivityRate() {
  return {
    type: actionType.REQUEST_ADDACTIVITYRATE
  }
}

function requestDelActivity() {
  return {
    type: actionType.REQUEST_DELACTIVITY
  }
}

function requestEditActivity() {
  return {
    type: actionType.REQUEST_EDITACTIVITY
  }
}

function requestEditProfilePic() {
  return {
    type: actionType.REQUEST_EDITPROFILEPIC
  }
}

function requestEditActivityPic() {
  return {
    type: actionType.REQUEST_EDITACTIVITYPIC
  }
}

function requestAddActivityCon() {
  return {
    type: actionType.REQUEST_ADDACTIVITYCON
  }
}

function requestAddActivity() {
  return {
    type: actionType.REQUEST_ADDACTIVITY
  }
}

function requestAddProfile(subreddit) {
  return {
    type: actionType.REQUEST_EDITPROFILE
  }
}

function requestEditProfile(subreddit) {
  return {
    type: actionType.REQUEST_EDITPROFILE
  }
}

export const REQUEST_LOGIN = 'REQUEST_LOGIN'
function requestLogin(subreddit) {
  return {
    type: actionType.REQUEST_LOGIN
  }
}

export const REQUEST_LOGOUT = 'REQUEST_LOGOUT'
function requestLogout(subreddit) {
  return {
    type: actionType.REQUEST_LOGOUT
  }
}

function requestProfile(subreddit) {
  return {
    type: actionType.REQUEST_PROFILE
  }
}

function requestActivity(subreddit) {
  return {
    type: actionType.REQUEST_ACTIVITY
  }
}

function requestIndActivity(subreddit) {
  return {
    type: actionType.REQUEST_INDACTIVITY
  }
}

export const REGISTER = 'REGISTER'
function receiveReg(json) {
  return {
    type: actionType.REGISTER,
    receivedAt: Date.now(),
    success: json.success
  }
}

//handle login
export const LOGIN = 'LOGIN'
function receiveLogin(auth, json) {
  return {
    type: actionType.LOGIN,
    auth,
    receivedAt: Date.now(),
    success: json.success,
    user: json.username,
    password: auth.value.password,
    msg: json.msg
  }
}

export const LOGOUT = 'LOGOUT'
function receiveLogout(auth, json) {
  return {
    type: actionType.LOGOUT,
    auth,
    user: 'guest',
    router: '/'
  }
}

function profileError() {
  return {
    type: actionType.PROFILEERROR
  }
}

function receiveProfile(profile, json) {
  return {
    type: actionType.PROFILE,
    profile,
    success: json.success,
    user: json.data.username,
    email: json.data.email,
    profile: json.data.Profile
  }
}

function receiveProfilePic(profile, json) {
  return {
    type: actionType.EDITPROFILEPIC,
    profile,
    success: json.success,
    profile: profile.media.Profile,
    user: profile.media.user
  }
}

function receiveActivityPic(profile, json) {
  return {
    type: actionType.EDITPROFILEPIC,
    profile,
    success: json.success,
    user: profile.user
  }
}

function receiveeditProfile(profile, json) {
  return {
    type: actionType.EDITPROFILE,
    profile,
    receivedAt: Date.now(),
    success: json.success,
    user: profile.value.user,
    profile: profile.value.Profile,
    newPassword: profile.value.newPassword,
    oldPassword: profile.value.oldPassword
  }
}

function receiveActivity(activity, json) {
  console.log(json);
  return {
    type: actionType.ACTIVITY,
    activity,
    success: json.success,
    activity: json.data,
    isLikedOrDisliked: json.isLikedOrDisliked
  }
}

function receiveIndActivity(activity, json) {
  return {
    type: actionType.INDACTIVITY,
    activity,
    success: json.success,
    activity: json.data,
    isLikedOrDisliked: json.isLikedOrDisliked
  }
}

function receiveIndActivityclear() {
  return {
    type: actionType.CLEARINDACTIVITY,
  }
}

function receiveAddActivity(activity, json) {
  return {
    type: actionType.ADDACTIVITY,
    activity,
    success: json.success,
    activityId: json.activityId,
    activity: activity.value,
  }
}

function receiveAddActivityCon(activity, json) {
  return {
    type: actionType.ADDACTIVITYCON,
    activity,
    rateSuccess: json.success,
    success: true,
    activity: activity.value.activity,
  }
}

function receiveEditActivity(activity, json) {
  return {
    type: actionType.EDITACTIVITY,
    activity,
    success: json.success,
    activity: activity.activity,
  }
}

function receiveAddActivityRate(activity, json) {
  return {
    type: actionType.ADDACTIVITYRATE,
    activity,
    rateSuccess: json.success,
    success: true,
    activity: activity.value.activity,
  }
}

function receiveDelActivity(activity, json) {
  return {
    type: actionType.DELACTIVITY,
    activity,
    success: json.success,
  }
}

export function authError(json) {
  return {
    type: actionType.AUTHERROR,
    success: json.success,
    msg: json.msg
  }
}

export function regError(json, close) {
  return {
    type: actionType.REGERROR,
    msg: json.msg,
    isOpenModel: !close
  }
}

export function rateError(json) {
  return {
    type: actionType.RATEERROR,
    success: json.success,
    msg: json.msg
  }
}

export function openLoginModal() {
  return {
    type: actionType.OPENLOGINMODAL
  }
}

export function closeLoginModal() {
  return {
    type: actionType.CLOSELOGINMODAL
  }
}

export function openRegModal() {
  return {
    type: actionType.OPENREGMODAL
  }
}

export function closeRegModal() {
  return {
    type: actionType.CLOSEREGMODAL
  }
}

export function openProfileModal() {
  return {
    type: actionType.OPENPROFILEMODAL
  }
}

export function closeProfileModal() {
  return {
    type: actionType.CLOSEPROFILEMODAL
  }
}

export function openProfilePicModal() {
  return {
    type: actionType.OPENPROFILEPICMODAL
  }
}

export function closeProfilePicModal() {
  return {
    type: actionType.CLOSEPROFILEPICMODAL
  }
}

export function openActivityModal() {
  return {
    type: actionType.OPENACTIVITYMODAL
  }
}

export function closeActivityModal() {
  return {
    type: actionType.CLOSEACTIVITYMODAL
  }
}

export function openActivityPicModal() {
  return {
    type: actionType.OPENACTIVITYPICMODAL
  }
}

export function closeActivityPicModal() {
  return {
    type: actionType.CLOSEACTIVITYPICMODAL
  }
}

export function openPicModal() {
  return {
    type: actionType.OPENPICMODAL
  }
}

export function closePicModal() {
  return {
    type: actionType.CLOSEPICMODAL
  }
}

export function login(auth) {

  return function (dispatch) {

    dispatch(requestLogin(auth))
    return fetch(Apipath_auth, {
      method: 'POST',
      credentials: 'include',
      body: JSON.stringify(auth.value),
      headers: {
        'Content-type': 'application/json'
      }

    })
      .then(response => response.json())
      .then(json => {
        //if login success
        if (json.success === true && json !== undefined) {
          dispatch(receiveLogin(auth, json))
        } else if (json.success === false) {
          dispatch(authError(json))
        }
      }

      )
    // catch any error in the network call.
  }
}

export function logout() {

  // Thunk middleware
  return function (dispatch) {
    return fetch(Apipath_auth, {
      method: 'DELETE',
      credentials: 'include',
      headers: {
        'Content-type': 'application/json'
      }

    })
      .then(response => response.json())
      .then(json => {
        //if login success
        if (json.success == true && json !== undefined) {
          dispatch(receiveLogout(json))
        }
      }

      )
    // catch any error in the network call.
  }
}

export function register(auth) {

  return function (dispatch) {

    dispatch(requestReg(auth))
    return fetch(Apipath_user, {
      method: 'POST',
      credentials: 'include',
      body: JSON.stringify(auth.value),
      headers: {
        'Content-type': 'application/json'
      }

    })
      .then(response => response.json())
      .then(json => {
        //if login success
        if (json.success == true && json !== undefined) {
          dispatch(receiveReg(auth, json))
        } else {
          dispatch(authError(json))
        }
      }
      )
  }
}

export function profile(profile) {

  return function (dispatch) {

    dispatch(requestProfile(profile))
    return fetch(Apipath_user, {
      method: 'GET',
      credentials: 'include',
      //body: JSON.stringify(profile.value),
      headers: {
        'Content-type': 'application/json'
      }

    })
      .then(response => response.json())
      .then(json => {
        //if login success
        if (json.success == true && json !== undefined) {
          dispatch(receiveProfile(profile, json))
        } else {
          dispatch(profileError(json, profile.close))
        }
      }

      )
    // catch any error in the network call.
  }
}

export function editprofile(profile) {

  return function (dispatch) {

    dispatch(requestEditProfile(profile))
    return fetch(Apipath_user, {
      method: 'PUT',
      credentials: 'include',
      body: JSON.stringify(profile.value),
      headers: {
        'Content-type': 'application/json'
      }

    })
      .then(response => response.json())
      .then(json => {
        //if login success
        if (json.success == true && json !== undefined) {
          dispatch(receiveeditProfile(profile, json))
        } else {
          dispatch(regError(json))
        }
      }

      )
    // catch any error in the network call.
  }
}

export function editProfilePic(profileValue) {

  var formData = new FormData();

  formData.append('media', profileValue.media.media[0]);

  return function (dispatch) {

    dispatch(requestEditProfilePic(profileValue))
    return fetch(Apipath_user + "/propic", {
      method: 'POST',
      credentials: 'include',
      body: formData
    })
      .then(response => response.json())
      .then(json => {
        //if login success
        if (json.success == true && json !== undefined) {
          dispatch(receiveProfilePic(profileValue, json))
          dispatch(profile(profileValue))
        } else {
          dispatch(regError(json))
        }
      }

      )
    // catch any error in the network call.
  }
}

export function editActivityPic(activityValue) {

  var formData = new FormData();

  formData.append('media', activityValue.media.media[0]);

  return function (dispatch) {

    dispatch(requestEditProfilePic(activityValue))
    return fetch(Apipath_activity + "/" + activityValue.eventID + "/content/" + activityValue.contentID + "/image", {
      method: 'POST',
      credentials: 'include',
      body: formData
    })
      .then(response => response.json())
      .then(json => {
        //if login success
        if (json.success == true && json !== undefined) {
          dispatch(receiveActivityPic(activityValue, json))
          dispatch(activity({
            value: '/' + activityValue.eventID
          }));
        } else {
          dispatch(regError(json))
        }
      }

      )
    // catch any error in the network call.
  }
}

export function activity(activity) {
  return function (dispatch) {
    dispatch(requestActivity(activity))
    return fetch(Apipath_activity + activity.value, {
      method: 'GET',
      credentials: 'include',
      headers: {
        'Content-type': 'application/json'
      }

    })
      .then(response => response.json())
      .then(json => {
        if (json.success == true || json !== undefined) {
          dispatch(receiveActivity(activity, json))
        } else if (json.success == false && json !== undefined) {
          dispatch(regError(json))
        }
      }

      )
    // catch any error in the network call.
  }
}

export function indactivity(activity) {
  return function (dispatch) {
    dispatch(receiveIndActivityclear())
    dispatch(requestIndActivity(activity))
    return fetch(Apipath_activity + activity.value, {
      method: 'GET',
      credentials: 'include',
      headers: {
        'Content-type': 'application/json'
      }

    })
      .then(response => response.json())
      .then(json => {
        if (json.success == true || json !== undefined) {
          dispatch(receiveIndActivity(activity, json))
        } else if (json.success == false && json !== undefined) {
          dispatch(regError(json))
        }
      }

      )
    // catch any error in the network call.
  }
}

export function addactivity(activity) {
  return function (dispatch) {
    dispatch(requestAddActivity(activity))
    return fetch(Apipath_activity, {
      method: 'POST',
      credentials: 'include',
      body: JSON.stringify(activity.value),
      headers: {
        'Content-type': 'application/json'
      }

    })
      .then(response => response.json())
      .then(json => {
        if (json.success == true && json !== undefined) {
          dispatch(receiveAddActivity(activity, json))
          //redirect the route
          const path = `/activity/${json.activityId}`
          browserHistory.push(path);
        } else if (json.success == false && json !== undefined) {
          dispatch(regError(json))
        }
      }
      )
    // catch any error in the network call.
  }
}

export function addactivityRate(activityValue) {
  return function (dispatch) {
    dispatch(requestAddActivityRate(activityValue))
    return fetch(Apipath_activity + activityValue.value.path, {
      method: 'POST',
      credentials: 'include',
      headers: {
        'Content-type': 'application/json'
      }

    })
      .then(response => response.json())
      .then(json => {
        if (json.success == true && json !== undefined) {
          dispatch(receiveAddActivityRate(activityValue, json))
          dispatch(activity({
            value: '/' + activityValue.value.id
          }));
        } else if (json.success == false) {
          dispatch(rateError(json))
        }
      }

      )
    // catch any error in the network call.
  }
}

export function addactivityCon(activityValue) {
  return function (dispatch) {
    dispatch(requestAddActivityCon(activityValue))
    return fetch(Apipath_activity + activityValue.value.path, {
      method: 'POST',
      credentials: 'include',
      body: JSON.stringify(activityValue.value.content),
      headers: {
        'Content-type': 'application/json'
      }

    })
      .then(response => response.json())
      .then(json => {
        if (json.success == true && json !== undefined) {
          dispatch(receiveAddActivityCon(activityValue, json))
          dispatch(activity({
            value: '/' + activityValue.value.id
          }));
        } else if (json.success == false) {
          dispatch(rateError(json))
        }
      }

      )
    // catch any error in the network call.
  }
}

export function editactivity(activityValue) {
  return function (dispatch) {

    dispatch(requestEditActivity(activityValue))
    return fetch(Apipath_activity + activityValue.value.path, {
      method: 'PUT',
      credentials: 'include',
      body: JSON.stringify(activityValue.value),
      headers: {
        'Content-type': 'application/json'
      }

    })
      .then(response => response.json())
      .then(json => {
        if (json.success == true && json !== undefined) {
          dispatch(receiveEditActivity(activityValue, json))
          dispatch(activity({
            value: '/' + activityValue.value._id
          }));
        } else if (json.success == false && json !== undefined) {
          dispatch(regError(json))
        }
      }

      )
    // catch any error in the network call.
  }
}

export function delactivity(activity) {
  return function (dispatch) {
    dispatch(requestDelActivity(activity))
    return fetch(Apipath_activity + activity.value.path, {
      method: 'DELETE',
      credentials: 'include',
      headers: {
        'Content-type': 'application/json'
      }

    })
      .then(response => response.json())
      .then(json => {
        if (json.success == true && json !== undefined) {
          dispatch(receiveDelActivity(activity, json))
        } else if (json.success == false && json !== undefined) {
          //dispatch(regError(json))
        }
      }

      )
    // catch any error in the network call.
  }
}
