/*
*  CSCI3100 - Software engineering Project 2016-2017
*  Project Name: ViewTango 
*  Group: 8 (Diamond 8)
*
*  Filename: viewActionType.js
*  Description: To define the action type of view function of the application.

*  All Rights Reserved.
*/

export const actionType = {
    REQUEST_VIEW: 'REQUEST_VIEW',
    RECEIVE_VIEW: 'RECEIVE_VIEW'
}
