/*
*  CSCI3100 - Software engineering Project 2016-2017
*  Project Name: ViewTango 
*  Group: 8 (Diamond 8)
*
*  Filename: LoginModal.js
*  Description: To define a login modal view

*  All Rights Reserved.
*/

import React from 'react';
import { render } from 'react-dom'
import { Button, ButtonToolbar, ButtonGroup, ControlLabel, FormControl, Col, Modal, Glyphicon } from 'react-bootstrap';

const LoginModal = React.createClass({

    //Handle user login
    handleLogin: function () {
        if (this.state.user === "" || this.state.password === "") {
            this.setState({
                missingMsg: "Please filling the missing information."
            })
        } else {
            this.setState({
                missingMsg: null
            })
            this.props.actions.login({
                type: 'LOGIN',
                value: this.state
            });
        }
    },

    //Handle the input compoent change
    handleChange: function (event) {
        if (event.target.id === 'username') {
            this.setState({
                user: event.target.value
            })
        } else if (event.target.id === 'password') {
            this.setState({
                password: event.target.value
            })
        }
    },

    getInitialState: function () {
        return {
            user: "",
            password: "",
            missingMsg: null
        }
    },
    
    //Render the Modal view
    render() {
        return (
            <Modal {...this.props}>
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-sm"><Glyphicon glyph="send" /> Login</Modal.Title>
                </Modal.Header>
                <Col xs={12} md={12}>
                    <form>
                        <br></br>
                        <ControlLabel>Username:</ControlLabel>
                        <FormControl
                            type="text"
                            id="username"
                            placeholder=""
                            onChange={this.handleChange}
                        />
                        <FormControl.Feedback />
                        <ControlLabel>Password:</ControlLabel>
                        <FormControl
                            type="password"
                            id="password"
                            placeholder=""
                            onChange={this.handleChange}
                        />
                        <FormControl.Feedback />
                        <p></p>
                        <ButtonToolbar>
                            <Button bsStyle="primary" bsSize="large" disabled={this.props.store.status == "logging in"} onClick={this.handleLogin}>Log in</Button>
                            <Button type="reset" bsSize="large">Clear</Button>
                        </ButtonToolbar>
                    </form>
                </Col>
                <Modal.Footer>
                    <h7 className="text-danger">{this.state.missingMsg === null ? this.props.store.msg : this.state.missingMsg}</h7>
                </Modal.Footer>
            </Modal>
        );
    }
});

export default LoginModal