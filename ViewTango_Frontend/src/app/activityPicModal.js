/*
*  CSCI3100 - Software engineering Project 2016-2017
*  Project Name: ViewTango 
*  Group: 8 (Diamond 8)
*
*  Filename: activityPicModal.js
*  Description: To define a add/edit picture modal view.

*  All Rights Reserved.
*/

import React from 'react';
import { render } from 'react-dom'
import { Button, ButtonToolbar, ButtonGroup, ControlLabel, FormControl, Col, Modal, Glyphicon } from 'react-bootstrap';
import Dropzone from 'react-dropzone';
import '../css/custom.css'
import { apiPath } from './apiPath'

//Define the path of API
var propicapipath = apiPath.propic

var dzstyle = {
    width: '100%',
    height: '100%',
    textAlign: 'center',
    border: '3px dashed rgb(245, 244, 243)'
}

const activityPicModal = React.createClass({

    getInitialState: function () {
        if (this.props.store.profile.propic) {
            return {
                filepath: "",
                user: "",
                password: "",
                missingMsg: null,
                file: [],
                function: "Upload your Profile Picture"
            }

        } else {
            return {
                user: "",
                password: "",
                missingMsg: null,
                file: [],
                filepath: null
            }
        }
    },

    onHandleUpload: function (res) {
        this.setState({
            file: res
        })
        this.props.actions.editActivityPic({
            type: 'Activity',
            media: {
                media: res,
                Profile: this.props.store.profile,
                user: this.props.store.user
            }
        });
    },

    render() {
        return (
            <Modal {...this.props}>
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-sm"><Glyphicon glyph="send" /> Upload your Activity Picture</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Dropzone style={dzstyle} onDrop={this.onHandleUpload.bind(this)} multiple="false" accept="image/jpeg">
                        {this.props.store.profile.propic ?
                            <div>
                                <div><img height="200" src={this.state.filepath} /></div>
                            </div>
                            : null
                        }
                        <div>
                            <h1><Glyphicon glyph="picture" /></h1>
                            <p>Dropping a Picture here, or click to select a Picture to upload.</p>
                        </div>
                    </Dropzone>
                </Modal.Body>
                <Modal.Footer>
                    <h7 className="text-danger">{this.state.missingMsg === null ? this.props.store.msg : this.state.missingMsg}</h7>
                </Modal.Footer>
            </Modal>
        );
    }
});

export default ProfilePicModal