/*
*  CSCI3100 - Software engineering Project 2016-2017
*  Project Name: ViewTango 
*  Group: 8 (Diamond 8)
*
*  Filename: profile.js
*  Description: To define a profile view

*  All Rights Reserved.
*/

// import library
import React from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { Panel, MenuItem, ProgressBar, ButtonGroup, DropdownButton, Image, img, Glyphicon, Table, thead, title, span, Well, Button, Jumbotron, ButtonToolbar, Form, FormGroup, InputGroup, FormControl, Modal, ControlLabel, Grid, Row, Col, head, ListGroupItem, ListGroup, Label } from 'react-bootstrap';;
import * as action from './action/action';
import { bindActionCreators } from 'redux';
import ProfileModal from './ProfileModal';
import ProfilePicModal from './ProfilePicModal';
import { apiPath } from './apiPath'

//Define the path of API
var propicapipath = apiPath.propic

var interest, gear = [];
var ProfileInstance = null;
const header = (<h4>Personal <small>Information</small></h4>);

const Profile = React.createClass({
    getInitialState: function () {
        var i = 0;
        interest = [];
        var loadingmodal = (
            <Modal.Dialog >
                <Modal.Body><h4>It just loading...</h4>
                    <ProgressBar active now={100} />
                </Modal.Body>
            </Modal.Dialog >
        );
        if (!this.props.store.profile && !this.props.store.email) {
            return {
                loadingProdfile: loadingmodal
            }
        } else {
            return {
                loadingProdfile: ''
            }
        }
    },

    componentWillMount() {
        this.props.actions.profile({
            type: 'PROFILE'
        });
    },

    getInterest() {
        var i = 0;
        interest = [];
        var space = "&nbsp;";
        for (i = 0; i < this.props.store.profile.interest.length; i++) {
            interest.push(null);
            interest.push(
                <span><Label bsStyle='success' >{this.props.store.profile.interest[i]}</Label>{" "}</span>
            );
        }
        gear = [];
        for (i = 0; i < this.props.store.profile.gear.length; i++) {
            gear.push(null);
            gear.push(
                <span><Label bsStyle='primary' >{this.props.store.profile.gear[i]}</Label>{" "}</span>
            );
        }

        // Handle icon
        var propic
        if (this.props.store.profile.propic !== "") {
            propic = propicapipath + this.props.store.profile.propic
        } else {
            propic = "src/assets/profile-icon.jpg"
        }

        this.state = {
            gear: gear,
            interest: interest,
            nickname: this.props.store.profile.nickname,
            description: this.props.store.profile.description,
            user: this.props.store.user,
            email: this.props.store.email,
            gender: this.props.store.profile.gender,
            propic: propic
        }
    },

    render() {
        if (this.props.store.profile && this.props.store.email) {
            this.getInterest();
            return (
                <div>
                    <Panel>
                        <Col xs={4}>
                            <center>
                                {}
                                <img src={this.state.propic} className="img-circle" alt="My Picture" width="200" height="200" circle />
                                <p></p>
                                <ButtonGroup vertical block>
                                    <Button id="PROFILE" bsStyle="primary" bsSize="Clear" onClick={() => this.props.actions.openProfilePicModal()} active>Upload Profile Picture</Button>
                                </ButtonGroup>
                            </center>
                        </Col>
                        <Col xs={8}>
                            <Col xs={12} md={12}>
                                <Panel>
                                    <ListGroup fill>
                                        <ListGroupItem>
                                            <span><h1>{this.state.user}<small> | Normal User</small></h1></span>
                                            <Table>
                                                <tbody>
                                                    <tr>
                                                        <td><h7><b>Interest</b></h7></td>
                                                        <td>{this.state.interest}</td>
                                                    </tr>
                                                    <tr>
                                                        <td><h7><b>Gear</b></h7></td>
                                                        <td>{this.state.gear}</td>
                                                    </tr>
                                                </tbody>
                                            </Table>
                                            <center>
                                                <ButtonGroup vertical block>
                                                    <Button bsStyle="primary" bsSize="Clear" onClick={() => this.props.actions.openProfileModal()} active>Edit Profile</Button>
                                                </ButtonGroup>
                                            </center>
                                        </ListGroupItem>
                                    </ListGroup>
                                </Panel>
                            </Col>
                            <Col xs={12} md={12}>
                                {header}
                                <Panel>
                                    <Form horizontal>
                                        <FormGroup>
                                            <Col xs={6}>
                                                <InputGroup>
                                                    <InputGroup.Addon>
                                                        <Glyphicon glyph="user" />
                                                    </InputGroup.Addon>
                                                    <FormControl type="text" value={this.state.user} />
                                                </InputGroup>
                                            </Col>
                                        </FormGroup>
                                        <FormGroup>
                                            <Col xs={6}>
                                                <InputGroup>
                                                    <InputGroup.Addon>
                                                        <Glyphicon glyph="apple" />
                                                    </InputGroup.Addon>
                                                    <FormControl type="text" value={this.state.nickname} />
                                                </InputGroup>
                                            </Col>
                                        </FormGroup>
                                        <FormGroup>
                                            <Col xs={4}>
                                                <InputGroup>
                                                    <InputGroup.Addon>
                                                        <Glyphicon glyph="heart" />
                                                    </InputGroup.Addon>
                                                    <FormControl type="text" value={this.state.gender} />
                                                </InputGroup>
                                            </Col>
                                        </FormGroup>
                                        <FormGroup>
                                            <Col xs={8}>
                                                <InputGroup>
                                                    <InputGroup.Addon>
                                                        <Glyphicon glyph="envelope" />
                                                    </InputGroup.Addon>
                                                    <FormControl type="text" value={this.state.email} />
                                                </InputGroup>
                                            </Col>
                                        </FormGroup>
                                        <FormGroup>
                                            <Col xs={12}>
                                                <InputGroup>
                                                    <InputGroup.Addon>
                                                        <Glyphicon glyph="info-sign" />
                                                    </InputGroup.Addon>
                                                    <FormControl type="text" value={this.state.description} />
                                                </InputGroup>
                                            </Col>
                                        </FormGroup>
                                    </Form>
                                </Panel>
                            </Col>
                            <ProfileModal show={this.props.store.openProfileModal} onHide={this.props.actions.closeProfileModal} actions={this.props.actions} store={this.props.store} />
                            <ProfilePicModal show={this.props.store.openProfilePicModal} onHide={this.props.actions.closeProfilePicModal} actions={this.props.actions} store={this.props.store} act="PROFILE" />
                        </Col>
                    </Panel>
                </div >
            )
        }
        else return (
            <div>{this.state.loadingProdfile}</div>
        );
    }
});

Profile.contextTypes = {
    store: React.PropTypes.object.isRequired
};

function mapStateToProps(state, ownProps) {
    return {
        store: state.combined.auth,
        view: state.combined.view
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(action, dispatch),
        viewActions: bindActionCreators(action, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
