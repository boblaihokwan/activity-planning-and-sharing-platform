/*
*  CSCI3100 - Software engineering Project 2016-2017
*  Project Name: ViewTango 
*  Group: 8 (Diamond 8)
*
*  Filename: category.js
*  Description: To define the category tab in view function.

*  All Rights Reserved.
*/

import React from 'react';
import ReactDOM from 'react-dom';
import { Tabs, Tab } from 'react-bootstrap'
import CategoryItem from './category/categoryItem';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as action from './action/action';
import * as viewAction from './action/viewAction';

var cat = ['City View', 'Venues', 'Sunrise/Sunset', 'Nightscape', 'Festival'];

class ActivityCategory extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    componentWillMount(){
        for(var i=0;i<cat.length;i++){
            this.props.viewActions.getView(cat[i]);
        }
    }

    render() {
        var tabView = []
        for(var i=0;i<cat.length;i++){
                tabView.push(
                    <Tab eventKey={i+1} title={cat[i]}><CategoryItem value={cat[i]} store={this.props.view}/></Tab>
                )
        }
        return (
            <Tabs defaultActiveKey={1} animation={false} id="noanim-tab-example">
                {tabView}
            </Tabs>
        );
    }
}

function mapStateToProps(state, ownProps) {
    return {
        store: state.combined.auth,
        view: state.combined.view
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(action, dispatch),
        viewActions: bindActionCreators(viewAction, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(ActivityCategory);
