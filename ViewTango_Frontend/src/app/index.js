/*
*  CSCI3100 - Software engineering Project 2016-2017
*  Project Name: ViewTango 
*  Group: 8 (Diamond 8)
*
*  Filename: index.js
*  Description: To define a react entry point of an application.

*  All Rights Reserved.
*/

import React from 'react';
import { Provider, subscribe } from 'react-redux'
import { render } from 'react-dom'
import { Router, Route, IndexRoute } from 'react-router'
import { connect } from 'react-redux';
import * as action from './action/action';
import Login from './login'
import Register from './register'
import Main from './main'
import store, { history } from './store/store'
import Homecontain from './homecontain'
import Profile from './profile'
import ViewCategory from './category'
import Activity from './activity'
import Timeline from './time'

const Parent = React.createClass({
    doSomething: function (value) {
        console.log('doSomething called by child with value:', value);
    },

    render: function () {
        const childrenWithProps = React.Children.map(this.props.children,
            (child) => React.cloneElement(child, {
                store: this.props.store
            })
        );

        return <div>{childrenWithProps}</div>
    }
});

class App extends React.Component {

    constructor() {
        super();
    }

    render() {
        store.subscribe(() => {
            console.log("Store updated", store.getState());
        });
        return (
            <Provider store={store}>
                <Router history={history} >
                    <Route path="/" component={Main}>
                        <IndexRoute component={Homecontain} />
                        <Route path="/profile" component={Profile} />
                        <Route path="/view" component={ViewCategory} />
                        <Route path="/activity" component={Activity} />
                        <Route path="/activity/:id" component={Timeline} />
                    </Route>
                </Router>
            </Provider>
        )
    }
}

render(<App />, document.getElementById('app'));