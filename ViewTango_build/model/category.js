var mongoose = require('mongoose');
var ObjectId = mongoose.Schema.Types.ObjectId;

// The database Schema of activity

var categorySchema = new mongoose.Schema({
	name: {type: String, unique: true, required: true }
});

var category = mongoose.model('category', categorySchema);

module.exports = category;
