var mongoose = require('mongoose');
var ObjectId = mongoose.Schema.Types.ObjectId;

// The database Schema of activity

var historySchema = new mongoose.Schema({
		activities: [ ObjectId ],
		Comments: [ ObjectId ],
		likes: [ ObjectId ],
});


var profileSchema = new mongoose.Schema({
		nickname: { type: String, default: "" },
		gender: { type: String, required: true},
		propic: { type: String, default: "" }, //url
		description: { type: String, default: "" },
		interest: [String],
		gear: [String] //gear used for photograph
});

var favSchema = new mongoose.Schema({
		activities: [ { type: ObjectId, ref: 'activity'} ]
});

var userSchema = new mongoose.Schema({
	username:{ //cannot change
		type: String,
		unique: true,
		required: true
	},
	email:{//cannot change
		type: String,
		unique: true,
		requried: true
	},
	password:{
		type: String,
		requried: true
	},
	usertype:{//cannot change
		type: String,
		default: "normal user"
	},
	createTime:{ type: Date, default: Date.now },//cannot change
	likes: {type: Number, default: 0},
	history: { type:historySchema, default: {} },
	Profile: { type:profileSchema, default: {} },
	followingUser: [ ObjectId ],
	followedUser: [ ObjectId ],
	fav: { type:favSchema, default: {}}
});




var user = mongoose.model('user', userSchema);

module.exports = user;
