var mongoose = require('mongoose');
var ObjectId = mongoose.Schema.Types.ObjectId;

// The database Schema of activity

var contentSchema = new mongoose.Schema({
    title: {type: String, required: true},
    description: { type: String, default: "" },
    imageUrl: {type: String, default: ""},
    // location: {type: locationSchema},
    time: {type: String, required: true}
},{
    timestamps: true
})

var commentsSchema = new mongoose.Schema({
	author: { type:ObjectId, ref: 'user'},
	content: { type: String, required: true},
	createTime: { type: Date, default: Date.now },
	likes: {type: Number, default: 0}
},{
    timestamps: true
});

var activitySchema = new mongoose.Schema({
    title: {type: String, required: true},
    author: { type: ObjectId, required: true, ref: 'user'},
    description: { type: String, default: "" },
    date: Date,
    peopleneed: {type: Number, default: 0},
    likes:{type: Number, default: 0},
    dislikes:{type: Number, default: 0},
    clickrate:{type: Number, default: 0},
    joiner: [{ type: ObjectId, ref: 'user'}],
    comments: [commentsSchema],
    contents: [contentSchema],
    categories: { type: String, default: "" },
    location: {type: String, required: true}
},{
    timestamps: true
})

module.exports = mongoose.model('activity', activitySchema);
