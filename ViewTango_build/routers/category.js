var express = require('express');
var router = express.Router();
var categoryModel = require('../model/category');
var activityModel = require('../model/activities');

// preseted category
var category = [
    'Venues',
    'Sunrice/Sunset',
    'Nightscape',
    'Festival',
    'City View'
];

router.get('/', function(req, res){ //return list of category
    categoryModel.find({name: req.body.category})
                .exec(function(err, docs){
                    if(err) throw err;

                    return res.json({
                        success: true,
                        msg: 'Query successfully.',
                        data: docs
                    });
                })
})

router.get('/viewList', function(req, res){ //return viewList of category
    activityModel.find({categories: req.query.category})
                .select('contents author title contents.imageUrl')
                .populate('author', 'username Profile.propic -_id')
                .exec(function(err, docs){
                    if(err) throw err;

                    return res.json({
                        success: true,
                        msg: 'Query successfully.',
                        data: docs
                    });
                })
})

module.exports = router;
