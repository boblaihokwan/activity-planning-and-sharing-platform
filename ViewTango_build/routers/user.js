var express = require('express');
var userModel = require('../model/user');
var activityModel = require('../model/activities');
var router = express.Router();
var bcrypt = require('bcrypt-nodejs');
var config = require('../config/config');
var fileupload = require('express-fileupload');
var decodeSession = require('../utils/auth').decodeSession;
var fs = require('fs');
const randomStr = require('../utils/randomStr');

var hidePrivacyData = {email: false, 'password': false, 'history': false, '__v': false};
var hidePassword = {'password': false, '__v': false};



router.use(function(req, res, next){ //middleware for decode token
	var token = req.session.token;
	console.log(token);
	if(token){
		req.decoded = token;
		next();
	}else{
		next();
	}
});

router.get('/list', function(req, res){ //return user list
	var hiding = {'_id': false,
				// 'password': false, //for debug use
				'fav._id': false,
				'Profile._id': false,
				'history._id': false,
				 '__v': false}

	if(false && req.decoded.usertype != "admin"){ //only for debug use
		return res.json({
			success: false,
			msg: 'You do not have the permission.'
		});
	}else{
		// config the sorted method
		var sorted = {'createTime': -1};
		if(req.query.sorted){
			switch(req.query.sorted){
				case 'dateAccending':
					sorted = {'createTime': -1};
					break;
				case 'dateDecending':
					sorted = 'createTime';
					break;
				case 'idAccending':
					sorted = {'_id': -1};
					break;
				case 'idDecending':
					sorted = '_id';
					break;
			}
		}

		console.log(sorted)

		userModel.find({}, hiding,
		{sort:sorted},
		function(err, results){
			res.json({
				success: true,
				msg: 'Query successfully',
				data: results
			});
		});
	}
});


router.get('/', function(req, res){ //return user according to login infomation
	console.log(req.query.sorted);
	var find = {};

	// list of infomation hiding for privacy and security
	var userHiding = {'_id': false,
				'password': false,
				'fav._id': false,
				'Profile._id': false,
				'history._id': false,
				 '__v': false};
	var activityOwnedHiding = {
		'__v': false,
		"contents": false,
		'comments': false
	};

	var activityNotOwnedHiding = {
		'__v': false,
		"contents": false,
		'comments': false,
		'joiner': false,
	};

	// check if logined
	if(req.decoded){
		find._id = req.decoded._id
	}else{
		return res.json({
			success: false,
			msg: 'Authentication is required.'
		})
	}

	userModel.findOne(find, userHiding)
		.populate('fav.activities')
		.exec(function(err, userDoc){
				userModel.populate(userDoc.fav.activities, {
					path: 'author',
					select: 'username Profile.propic -_id',
				});

				// get the related activity
				activityModel.find({author: req.session.token._id}, activityOwnedHiding)
					.populate('author', 'username Profile.propic -_id')
					.exec(function(err, actDoc){
						if(err) throw err;

						activityModel.find({joiner: req.session.token._id}, activityNotOwnedHiding)
							.populate('author', 'username Profile.propic -_id')
							.exec(function(err, joinActDoc){
								if(err) throw err;

								res.json({
									success: true,
									msg: 'Query successfully',
									data: userDoc,
									activity: actDoc,
									joined: joinActDoc
								});

							})
					})
		});
});

router.get('/:username', function(req, res){ //return user according to the id
	var hiding = hidePrivacyData;
	if(req.decoded && req.decoded._id == req.params.id){
		hiding = hidePassword;
	}else if(req.query.type === "admin"){
		hiding  = {};
	}

	userModel.findOne({
		username:req.params.username
	}, hiding, function(err, user){
		res.json({
				success: true,
				msg: 'Query successfully',
				data: user
		});
	})
});

// create new user
router.post('/', function(req, res){
	//check field empty
	req.checkBody('email', 'Invalid email').notEmpty().isEmail();
	req.checkBody('password', 'Require password').notEmpty();
	req.checkBody('username', 'Require username').notEmpty();
	req.checkBody('gender', 'Wrong gender').notEmpty().matches(/^[MF]$/g);

	var errors = req.validationErrors();
	if(errors){
		console.log(errors);
		var respond = {
			success: false,
			msg: errors
		}
		res.json(respond);
	}else{
		var salt = bcrypt.genSaltSync(10);
		var hash = bcrypt.hashSync(req.body.password, salt)
		var newuser = new userModel({
			username: req.body.username,
			email: req.body.email,
			password: hash,
			Profile:{
				gender: req.body.gender
			}
		})
		// if(req.body.usertype){
		// 	newuser.usertype = req.body.usertype;
		// }

		newuser.save(function(err){
			if(err){
				if(err.errors && err.errors['Profile.gender']){
					console.log(err)
					return res.json({
						success: false,
						msg: 'Wrong gender format'
					});
				}else{
					return res.json({
						success: false,
						msg: 'Email or username has been used'
					});
				}
			}else{
				return res.json({
					success: true,
					msg: 'Registration successful.'
				})
			}
		})
	}
});

var findUser = function(req, res){ //modify the user
	if(!req.decoded){
		res.json({
			success: false,
			msg: 'Authentication is required.'
		})
	}else if(req.params.username
			&& req.decoded.username != req.params.username
			&& req.decoded.usertype != 'admin'){
		res.json({
			success: false,
			msg: 'Permission denied'
		})
	}else{
		if(req.params && req.params.username){
			var userquery = req.params.username;
		}else{
			var userquery = req.decoded.username;
		}

		console.log(userquery);
		userModel.findOne({
			username: userquery
		}, function(err, user){
			if(!user) return res.json({
					success: false,
					msg: 'user not exist.'
				})

			var updateBody = {
				Profile: user.Profile ? Object.assign(user.Profile, {}) : {}
			}

			updateBody.Profile._id = undefined;
			var toUpdate = false;
			if(req.body.newPassword && req.body.oldPassword){ //change passsword
				if(bcrypt.compareSync(req.body.oldPassword, user.password)){
					var salt = bcrypt.genSaltSync(10);
					var hash = bcrypt.hashSync(req.body.newPassword, salt);
					updateBody.password = hash;
					toUpdate = true;
				}else{
					return res.json({
						success: false,
						msg: 'old password is not match.'
					})
				}
			}

			if(req.body.Profile){
				updateBody.Profile = Object.assign(updateBody.Profile, req.body.Profile);
				toUpdate = true;
			}

			console.log(updateBody)
			if(!toUpdate){
				res.json({
					success: false,
					msg: 'Nothing is requested to be updated.'
				})
			}else{
				userModel.findOneAndUpdate({username: userquery}, { $set: updateBody }, {upsert: true}
				, function(err, doc){
					if(err){
						console.log(err);
						res.json({
							success: false,
							msg: 'update user '+ userquery + ' error.'
						})
					}else{
						console.log(doc)
						userModel.find
						res.json({
							success: true,
							msg: 'Modify user '+ userquery + ' success.'
						})
					}
				});
			}
		})
	}
}
router.put('/:username', findUser);
router.put('/', findUser);

var propicPath = config.public + 'propic/';

router.post('/propic',fileupload(), function(req, res){
	console.log(req.files)
	if(!req.decoded){
		return res.json({
			success: false,
			msg: 'Authentication is required.'
		})
	}else{
		if(req.files.media && req.files.media.mimetype == 'image/jpeg'){ //accept jpeg only for smaller storage usage
			do{
				var fileName = randomStr() + '.jpg'
			}while(fs.existsSync(propicPath + fileName))

			req.files.media.mv(propicPath + fileName, function(err){
				if(err) return res.status(500).json({
					success: false,
					msg: err,
					path: propicPath + fileName,
				});

				userModel.findOneAndUpdate({_id: req.decoded._id}, { $set: { "Profile.propic": fileName}}
					, {upsert: 1}, function(err, doc){
						if(err) return res.status(500).json({
							success: false,
							msg: err
						});

						if(doc.Profile.propic !== ''){
							fs.unlink(propicPath + doc.Profile.propic);
						}

						return res.json({
							success: true,
							msg: 'upload success'
						})
					})


			})
		}else{
			res.status(500).json({
				success: false,
				msg: 'file error'
			});
		}
	}
})

router.delete('/:username', function(req, res){ //delete the user, only admin can do, last to do
	if(!req.decoded){
		res.json({
			success: false,
			msg: 'Token is required.'
		})
	}
});

module.exports = router;
