var express = require('express');
var router = express.Router();
var activityModel = require('../model/activities');
var userModel = require('../model/user');
var decodeSession = require('../utils/auth').decodeSession;
var objIdChk = require('../utils/objIdChk');
var fileupload = require('express-fileupload');
var fs = require('fs');
const randomStr = require('../utils/randomStr');
var config = require('../config/config');

var normalInfoHide = {}
var owneInfoHide = {}
var ContentSort = {}

// middleware to check if the activity is owned by the user
function chkOwner(req, res, next){
    activityModel.findOne({_id: req.params.id, author: req.session.token._id})
        .exec(function(err, doc){
            if(err) throw err;

            if(doc){
                next();
            }else{
                return res.status(403).json({
                    success: false,
                    msg: 'Permission denied: You are not the owner.'
                })
            }
        })
}

// get list of activity
router.get('/', function(req, res){
    var options = {
        latest: {createdAt: -1},
        mostViewed: {clickrate: -1}
    }

    // determine the sorted method
    var option = options.latest;
    if(req.query.sort == 'mostViewed'){
        option = options.mostViewed;
    }

    activityModel.find({})
        .sort(option)
        .select('-__v -comments')
        .populate('author', 'username Profile.propic -_id')
        .exec(function(err, docs){
            if(err) throw err;

            return res.json({
                suceess: true,
                msg: 'Query successfully.',
                data: docs
            })
        })
})

//search activity
router.get('/search', function(req, res){
    req.checkQuery('keyword', 'Require keyword').notEmpty();

	var errors = req.validationErrors();
	if(errors){
		console.log(errors);
		var respond = {
			success: false,
			msg: errors
		}
		return res.json(respond);
	}

    // create regex for searching
    var reg = new RegExp("\\b(" + req.query.keyword + ")\\b", "g");
    var catReg = new RegExp("^(" + req.query.keyword + ")$", "g");
    activityModel.find({title: { "$regex": reg, $options: 'i' }})
				.limit(10)
                .select('-__v')
				.populate('author', 'username Profile.propic -_id')
				.exec(function(err, docs){
					if(err) throw err;
					return res.json({
						success: true,
						msg: 'Query successfully',
						data: docs
					})
				});
})

// get the activity with the id
router.get('/:id', objIdChk('id'),function(req, res){
    activityModel.findOne({_id: req.params.id })
				.select('-__v')
				.populate('author', 'username Profile.propic -_id')
				.populate('comments.author', 'username Profile.propic -_id')
				.exec(function(err, actDoc){
					if(err) throw err;

                    if(!actDoc) return res.json({ // if it is not found
                        success: true,
                        msg: 'Query successfully',
                        data: actDoc
                    })

                    if(actDoc.contents){ // sort the time line content in the activity
                        actDoc.contents.sort(function(x, y){
                            var toIntfunc = function(str){
                                var temp = str.split(':');
                                return parseInt(temp[0]+temp[1]);
                            }
                            return toIntfunc(x.time) - toIntfunc(y.time);
                        });
                    }


                    actDoc.clickrate += 1; //add the click rate
                    actDoc.save(function(err){ //save the updated clickrate and content order
                        if(err) throw err;

                        if(req.session.token){ // if logined
    						userModel.findOne({_id: req.session.token._id})
    							.exec(function(err, doc){
    								if(err) throw err;
                                    var isFavourited = false
                                        ,isLikedOrDisliked = false

                                    if(doc.fav.activities.indexOf(req.params.id) != -1){ //check if the user favourite the activity
                                        isFavourited = true;
                                    }

                                    if(doc.history.likes.indexOf(req.params.id) != -1){ //check if the user like the activity
                                        isLikedOrDisliked = true;
                                    }

                                    return res.json({
                                        success: true,
                                        msg: 'Query successfully',
                                        data: actDoc,
                                        isFavourited: isFavourited,
                                        isLikedOrDisliked: isLikedOrDisliked
                                    })

    							})
    					}else{
    						return res.json({
    							success: true,
    							msg: 'Query successfully',
    							data: actDoc
    						})
    					}
                    })

				});
})

// like the activity
router.post('/:id/like', decodeSession, objIdChk('id'), function(req, res){
    userModel.findOne({_id: req.session.token._id})
		 .exec(function(err, userDoc) {
		 	if(err) throw err;

            //check if the user has liked or dislikeed the activity
            // reject request if user has liked or dislikeed
			if(userDoc.history.likes.indexOf(req.params.id) != -1){
				return res.json({
					success: false,
					msg: 'You cannot like or dislike the same activity again!'
				})
			}else{
				activityModel.update({_id: req.params.id, author: { $ne: req.session.token._id }} //reject request if the user is owner
					,{ $inc: {likes: 1}})
					.exec(function(err, result){
						if(err) throw err;

						console.log(result);
						if(result.n == 0){
							res.json({
								success: false,
								msg: 'You cannot like your own activity!'
							})
						}else{
                            userModel.findByIdAndUpdate(req.session.token._id // update the like or dislike history of the user
            					, { $push: {"history.likes": req.params.id}}
            					, { safe: true, upsert:true}
            					, function(err, doc){
            						if(err) throw err;
            						return res.json({
            							success: true,
            							msg: 'like activity thread ' + req.params.id + ' successfully.'
            						});
            					})
						}
					})
			}
		 })
})

// dislike the activity
router.post('/:id/dislike', decodeSession, objIdChk('id'), function(req, res){
    userModel.findOne({_id: req.session.token._id})
		 .exec(function(err, userDoc) {
		 	if(err) throw err;

            //check if the user has liked or dislikeed the activity
            // reject request if user has liked or dislikeed
			if(userDoc.history.likes.indexOf(req.params.id) != -1){
				return res.json({
					success: false,
					msg: 'You cannot like or dislike the same activity again!'
				})
			}else{
				activityModel.update({_id: req.params.id, author: { $ne: req.session.token._id }} //auto reject request if the user is owner
					,{ $inc: {dislikes: 1}})
					.exec(function(err, result){
						if(err) throw err;
                        console.log(2);
						console.log(result);
						if(result.n == 0){
							res.json({
								success: false,
								msg: 'You cannot like your own activity!'
							})
						}else{
                            userModel.findByIdAndUpdate(req.session.token._id  // update the like or dislike history of the user
            					, { $push: {"history.likes": req.params.id}}
            					, { safe: true, upsert:true}
            					, function(err, doc){
            						if(err) throw err;
            						return res.json({
            							success: true,
            							msg: 'dislike activity thread ' + req.params.id + ' successfully.'
            						});
            					})
						}
					})
			}
		 })
})

// add comment to the activity
router.post('/:id/comment', decodeSession, objIdChk('id'), function(req, res){
    req.checkBody('content', 'Require content').notEmpty();
    var errors = req.validationErrors();
    if(errors){ // check if the field is valid
        console.log(errors);
        var respond = {
            success: false,
            msg: errors
        }
        return res.json(respond)
    }

    var updateBody = {
        author: req.session.token._id,
        content: req.body.content
    }

    activityModel.findOneAndUpdate({_id: req.params.id} // add comments
        , {$push: {comments: updateBody}}
        , { safe: true, upsert:true}
        , function(err, result){
            userModel.update({_id: req.session.token._id},
                { $push: {'history.PictureComments': req.params.id}},
                { safe: true, upsert:true},
                function(err, result){
                    return res.json({
                        success: true,
                        msg: 'Add comment at activity ' + req.params.id + 'success.'
                    });
                })

        })
})

//for owner

//create new activity
router.post('/', decodeSession, function(req, res){

    //check if the field is valid
    req.checkBody('title', 'Invalid title').notEmpty();
    req.checkBody('peopleneed', 'Invalid poeple need').notEmpty().isInt();
    req.checkBody('date', 'Invalid date').notEmpty().isDate();
    req.checkBody('location', 'Invalid location').notEmpty();

    var errors = req.validationErrors();
	if(errors){
		console.log(errors);
		var respond = {
			success: false,
			msg: errors
		}
		return res.json(respond);
	}

    // create new activity
    var newActivity = new activityModel({
        title: req.body.title,
        peopleneed: req.body.peopleneed,
        date: req.body.date,
        author: req.session.token._id,
        description: req.body.description || "",
        contents: req.body.contents || [],
        categories : req.body.categories || [],
        location: req.body.location
    });

    newActivity.save(function(err, doc){ // save the activity
        if(err){
            console.log(err);
            return res.json({
                success: false,
                msg: errors
            });
        }else{
            return res.json({
                success: true,
                msg: 'create new activity success!',
                activityId: doc._id
            });

        }
    })
})

// add the new timeline content to the activity
router.post('/:id/content/', decodeSession, objIdChk('id'), chkOwner, function(req, res){

    //check if the field is valid
    req.checkBody('title', 'Invalid title').notEmpty();
    req.checkBody('time', 'Invalid time').notEmpty();

    var errors = req.validationErrors();
    if(errors){
        console.log(errors);
        var respond = {
            success: false,
            msg: errors
        }
        return res.json(respond);
    }

    // create new timeline content
    var content = {
        title: req.body.title,
        time: req.body.time,
        description: req.body.description || ""
    }

    activityModel.findOneAndUpdate({_id: req.params.id}, // save the timeline content
        {$push: {'contents': content}},
        {safe: true, upsert: true})
        .exec(function(err, result){
            if(err) throw err;

            return res.json({
                suceess: true,
                msg: 'Add content successfully.'
            })
        })
})

//change the timeline content
router.put('/:id/content/:contentId'
, decodeSession
, objIdChk(['id', 'contentId'])
, chkOwner
, function(req, res){
    var update = {};

    // check the requested field
    req.body.title ? update['contents.$.title'] = req.body.title: "";
    req.body.time ? update['contents.$.time'] = req.body.time: "";
    req.body.description ? update['contents.$.description'] = req.body.description: "";

    // update the content
    activityModel.update({_id: req.params.id, 'contents._id': req.params.contentId},
        {$set: update})
        .exec(function(err, result){
            if(err) throw err;

            return res.json({
                suceess: true,
                msg: 'update content successfully.'
            })
        })
})

//uploading image
const imagePath = config.public + 'activity/'

router.post('/:id/content/:contentId/image'
, decodeSession
, objIdChk(['id', 'contentId'])
, chkOwner
, fileupload()
, function(req, res){
    if(req.files.media && req.files.media.mimetype == 'image/jpeg'){ //accept jpeg only for smaller storage usage
        //prevent dulplcated file name
        do{
            var fileName = randomStr() + '.jpg'
        }while(fs.existsSync(imagePath + fileName))

        // move the file
        req.files.media.mv(imagePath + fileName, function(err){
            if(err) return res.status(500).json({
                success: false,
                msg: err,
                path: imagePath + fileName,
            });

            // add image file data
            activityModel.findOneAndUpdate({_id: req.params.id, 'contents._id': req.params.contentId},
                {$set: {'contents.$.imageUrl': fileName }}
                , {upsert: 1}, function(err, doc){
                    if(err) return res.status(500).json({
                        success: false,
                        msg: err
                    });

                    // delete the old file
                    var content;
                    for(var i=0;i<doc.contents.length;i++){ // find the corresponding content
                        var temp = doc.contents[i];
                        if(temp._id == req.params.contentId){
                            content = temp;
                            break;
                        }
                    }

                    // unlink the old image file
                    if(content.imageUrl !== ''){
                        fs.unlink(imagePath + content.imageUrl);
                    }

                    return res.json({
                        success: true,
                        msg: 'upload success'
                    })
                })


        })
    }else{
        res.status(500).json({
            success: false,
            msg: 'file error'
        });
    }
})

// delete the content
router.delete('/:id/content/:contentId', decodeSession
, objIdChk(['id', 'contentId'])
, chkOwner
, function(req, res){
    activityModel.update({_id: req.params.id},
        {$pull: {contents: {_id: req.params.contentId}}})
        .exec(function(err, result){
            if(err) throw err;

            return res.json({
                suceess: true,
                msg: 'Delete content successfully.'
            })
        })
})

// change the infomation of the activity
router.put('/:id', decodeSession, objIdChk('id'), chkOwner, function(req, res){
    // preseted valid update field
    var validField = ['title', 'description', 'peopleneed', 'date', 'categories', 'location', 'contents'];
    var updateBody = {};

    // check if the field is valid
    if(req.body.peopleneed)
        req.checkBody('peopleneed', 'Invalid poeple need').isInt();

    if(req.body.date)
        req.checkBody('date', 'Invalid date').isDate();

    var errors = req.validationErrors();
	if(errors){
		console.log(errors);
		var respond = {
			success: false,
			msg: errors
		}
		return res.json(respond);
	}

    // check if the field is exist
    for(var i=0;i<validField.length; i++){
        var field = validField[i];
        if(req.body[field]){
            updateBody[field] = req.body[field];
        }
    }

    // update the activity
    activityModel.update({
        _id: req.params.id,
        author: req.session.token._id
    },
    updateBody,
    {safe: true, upsert: true},
    function(err, result){
        if(err) throw err;

		return res.json({
			success: true,
			msg: 'update activity ' + req.params.id + ' successfully.'
		})
    })
})

// delete the activity
router.delete('/:id', decodeSession, objIdChk('id'), chkOwner, function(req, res){
    activityModel.findOne({_id: req.params.id}).remove()
        .exec(function(err){
            if(err) throw err;

            return res.json({
                suceess: false,
                msg: 'delete activity ' + req.params.id + ' successfully.'
            })
        })
})

module.exports = router;
