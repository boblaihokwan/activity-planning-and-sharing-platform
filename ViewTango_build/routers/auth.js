var express = require('express');
var userModel = require('../model/user');
var router = express.Router();
var bcrypt = require('bcrypt-nodejs');
var config = require('../config/config')
var jwt = require('jsonwebtoken');


// login function
router.post('/', function(req, res){
	// check if the field is valid
	req.checkBody('user', 'Require user infomation').notEmpty();
	req.checkBody('password', 'Require password').notEmpty();

	console.log(req.body);


	var errors = req.validationErrors();
	if(errors){
		console.log(errors);
		var respond = {
			success: false,
			msg: errors
		}
		res.json(respond);
	}else{
		// get the user infomation
		userModel.findOne({
			$or: [{email: req.body.user}, {username: req.body.user}]
		}, function(err, user){
			console.log(user)
			if(err) throw err; // db error

			if(!user){ //user not find
				console.log("no user")
				return res.json({
					success: false,
					msg: "Wrong password or invalid email"
				})
			}else{
				//compare pw
				var userData = {
					_id: user._id,
					username: user.username,
					usertype: user.usertype
				}

				if(bcrypt.compareSync(req.body.password, user.password)){
					req.session.token = userData;

					res.json({
						success: true,
						msg: 'authentication successful',
						username: user.username
					})
				}else{
					res.json({
						success: false,
						msg: "Wrong password or invalid email"
					})
				}
			}
		})
	}
});

// logout function
router.delete('/', function(req, res){
	console.log(req.session.token)

	// check if logined
	if(!req.session.token){
		res.json({
			success: false,
			msg: "You have not login."
		})
	}else{
		req.session.token = undefined; // delete the session data

		res.json({
			success: true,
			msg: "Logout successful."
		});
	}
});

module.exports = router;
