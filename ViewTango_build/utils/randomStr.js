// random string generator for image file name
module.exports = function makeid(){
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < 10; i++ ) //10 char only
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}
