var mongoose = require('mongoose');

module.exports = function(params){
    // get field to check
    if(params instanceof Array){
        var fields = params;
    }else{
        var fields = [params];
    }


    return function(req, res, next){
        for(var i=0;i<fields.length;i++){
            var field = fields[i];
            console.log(req.params[field], field);
            if(mongoose.Types.ObjectId.isValid(req.params[field])){ // check if the id is valid mongodb object id
                continue;
            }else{
                return res.json({
                    success: false,
                    msg: 'Invalid id'
                })
            }
        }

        next();
    }
}
