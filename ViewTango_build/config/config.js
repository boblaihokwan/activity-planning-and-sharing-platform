// The config value store
// it is capatible to run in openshift environment or local environment
// The real config value will determined during runtime

module.exports = {
	'secret': 'YdI6SGzts7lDzT1KzTeB8xCILj3H4XDmrVUZPzdWhyPyZjnXCyKdtaR1esKXneO7Fy9DjzNryJ4ueX9Xf9eHsJPtM2qhwxDI8z67',
	'mongodbUrl': (process.env.OPENSHIFT_MONGODB_DB_URL ||'mongodb://localhost/') + 'viewtango',
	'port': process.env.OPENSHIFT_NODEJS_PORT || 8080,
	'ip': process.env.OPENSHIFT_NODEJS_IP || '',
	'public': process.env.OPENSHIFT_DATA_DIR || (__dirname + '/../public/'),
	'sessionSecret': '6igXjJLGAl0sQ9lKQKB9F3pjIwtwnZ5PjZtcLoaMEko9CVKNXIlYDKsh07FUOTKlAabAb08xL1t77fmDnejqxeAiO',
	'view': './view/'
}
