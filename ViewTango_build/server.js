var express = require('express');
var session = require('express-session');
var validator = require('express-validator');
var bodyparser = require('body-parser');
var cookieparser = require('cookie-parser');
var cookieSession = require('cookie-session');
var mongoose = require('mongoose');
var config = require('./config/config');
var cors = require('cors')
var path = require('path');

var mongoUrl = config.mongodbUrl;
mongoose.connect(mongoUrl);

var app = express();
var port = config.port;
var ip = config.ip;

app.use(cookieSession({
	name: 'session',
	keys: [config.sessionSecret],
	maxAge: 24 * 60 * 60 * 1000
}));


app.use(bodyparser.json());
app.use(validator());

app.use(cookieparser());

//add routers...
var userRouter = require('./routers/user');
var authRouter = require('./routers/auth');
var categoryRouter = require('./routers/category');
var activitiesRouter = require('./routers/activities');
var viewExplorerRouter = require('./routers/viewExplorer');

var favicon = require('serve-favicon');

// solve the cors problem
app.use(cors({
	origin: ['http://localhost:8080', 'http://192.168.56.101:8080'],
	methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
	allowedHeaders: "Content-Type",
	credentials: true,
	preflightContinue: true
}));

// request logger
app.use(function(req, res, next){
    console.log("get req at " + req.url + " " + req.method);
    console.log(req.body);
    console.log('');
    next();
})

// serve static content, i.e the image
app.use('/public', express.static(config.public));


//add router to the express server
app.use('/api/user', userRouter);
app.use('/api/auth', authRouter);
app.use('/api/category', categoryRouter);
app.use('/api/activity', activitiesRouter);

app.use('/', express.static(config.view));

// to serve the react html file
var serveHTML = function(req, res){
	res.sendfile(config.view + 'index.html');
}

app.use('/activity/', serveHTML);
app.use('/activity/:id', serveHTML);
app.use('/profile/', serveHTML);
app.use('/profile/', serveHTML);
app.use('/view/', serveHTML);

app.listen(port, ip, function(err){
	if (err) throw err;
	console.log("server listen at", ip, port);
});
