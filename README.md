# ViewTango #
---
## Folder structure ##
1. ViewTango_Frontend: react.js source code
2. ViewTango_build: node.js server source code with compiled bundle.js

## System requirement ##
* node.js 7.7.1
* mongodb 2.4.9
* react *